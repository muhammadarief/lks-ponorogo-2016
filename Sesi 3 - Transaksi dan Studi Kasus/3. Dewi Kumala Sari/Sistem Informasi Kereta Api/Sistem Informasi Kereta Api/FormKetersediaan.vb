﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FormKetersediaan

    Private Sub FormKetersediaan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bukaDB()

        Dim CMD1 = New SqlCommand("SELECT * from [kereta]", konek)
        Dim DR1 = CMD1.ExecuteReader
        While DR1.Read
            ComboBox1.Items.Add(DR1.Item("nama_kereta"))
        End While
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        DA = New SqlDataAdapter("select count(*) from kursi where status_kursi=0 and id_kereta=(select id_kereta from [kereta] where nama_kereta='" & ComboBox1.SelectedItem & "')", konek)
        Dim srt = New DataTable
        srt.Clear()
        DA.Fill(srt)
        DataGridView1.Datasource = srt
        DataGridView1.Refresh()
    End Sub

    Private Sub GantiPasswordToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GantiPasswordToolStripMenuItem.Click
        FormGantiPassword.Show()
    End Sub

    Private Sub TambahKeretaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahKeretaToolStripMenuItem.Click
        FormTambahKereta.Show()
        Me.Dispose()
    End Sub

    Private Sub TambahStasiunToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahStasiunToolStripMenuItem.Click
        FormTambahStasiun.Show()
        Me.Dispose()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        FormTambahUser.Show()
        Me.Dispose()

    End Sub

    Private Sub TambahJadwalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahJadwalToolStripMenuItem.Click
        FormTambahJadwal.Show()
        Me.Dispose()
    End Sub

    Private Sub DataPenumpangToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DataPenumpangToolStripMenuItem.Click
        FormTambahPenumpang.Show()
        Me.Close()
    End Sub

    Private Sub TransaksiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TransaksiToolStripMenuItem.Click
        FormTransaksi.Show()
        Me.Dispose()

    End Sub

    Private Sub BulananToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BulananToolStripMenuItem.Click
        FormLaporan.Show()
    End Sub

    Private Sub LogOutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LogOutToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan Log Out?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            FormLogin.Show()
            Me.Dispose()
        End If
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KeluarToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan keluar?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            Me.Close()
        End If
    End Sub

    Private Sub DataKetersediaanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DataKetersediaanToolStripMenuItem.Click
        FormKetersediaan.Show()
    End Sub
End Class