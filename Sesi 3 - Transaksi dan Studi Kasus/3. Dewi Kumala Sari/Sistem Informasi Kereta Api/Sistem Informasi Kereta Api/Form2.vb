﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class Form2

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bukaDB()
        CMD = New SqlCommand("SELECT * FROM [user] where username='" & FormLogin.TextBox1.Text & "'", konek)
        DR = CMD.ExecuteReader
        If DR.HasRows() Then
            If DR.Read Then
                Label5.Text = DR.Item("id_akses")
            End If
        End If
        AkunToolStripMenuItem.Text = FormLogin.TextBox1.Text

        If Label5.Text = 1 Then
            GantiPasswordToolStripMenuItem.Enabled = True
            ToolStripMenuItem1.Enabled = True
            LogOutToolStripMenuItem.Enabled = True
            KeluarToolStripMenuItem.Enabled = True
            TambahKeretaToolStripMenuItem.Enabled = True
            TambahStasiunToolStripMenuItem.Enabled = True
            TambahJadwalToolStripMenuItem.Enabled = True
            DataKetersediaanToolStripMenuItem.Enabled = True
            DataPenumpangToolStripMenuItem.Enabled = True
            TransaksiToolStripMenuItem.Enabled = True
            HarianToolStripMenuItem.Enabled = True
            BulananToolStripMenuItem.Enabled = True
            TahunanToolStripMenuItem.Enabled = True
        ElseIf Label5.Text = 2 Then
            GantiPasswordToolStripMenuItem.Enabled = True
            ToolStripMenuItem1.Enabled = False
            LogOutToolStripMenuItem.Enabled = True
            KeluarToolStripMenuItem.Enabled = True
            TambahKeretaToolStripMenuItem.Enabled = True
            TambahStasiunToolStripMenuItem.Enabled = True
            TambahJadwalToolStripMenuItem.Enabled = True
            DataKetersediaanToolStripMenuItem.Enabled = False
            DataPenumpangToolStripMenuItem.Enabled = False
            TransaksiToolStripMenuItem.Enabled = False
            HarianToolStripMenuItem.Enabled = False
            BulananToolStripMenuItem.Enabled = False
            TahunanToolStripMenuItem.Enabled = False
        ElseIf Label5.Text = 3 Then
            GantiPasswordToolStripMenuItem.Enabled = True
            ToolStripMenuItem1.Enabled = False
            LogOutToolStripMenuItem.Enabled = True
            KeluarToolStripMenuItem.Enabled = True
            TambahKeretaToolStripMenuItem.Enabled = False
            TambahStasiunToolStripMenuItem.Enabled = False
            TambahJadwalToolStripMenuItem.Enabled = False
            DataKetersediaanToolStripMenuItem.Enabled = True
            DataPenumpangToolStripMenuItem.Enabled = True
            TransaksiToolStripMenuItem.Enabled = True
            HarianToolStripMenuItem.Enabled = False
            BulananToolStripMenuItem.Enabled = False
            TahunanToolStripMenuItem.Enabled = False
        ElseIf Label5.Text = 4 Then
            GantiPasswordToolStripMenuItem.Enabled = True
            ToolStripMenuItem1.Enabled = False
            LogOutToolStripMenuItem.Enabled = True
            KeluarToolStripMenuItem.Enabled = True
            TambahKeretaToolStripMenuItem.Enabled = True
            TambahStasiunToolStripMenuItem.Enabled = False
            TambahJadwalToolStripMenuItem.Enabled = False
            DataKetersediaanToolStripMenuItem.Enabled = True
            DataPenumpangToolStripMenuItem.Enabled = True
            TransaksiToolStripMenuItem.Enabled = True
            HarianToolStripMenuItem.Enabled = False
            BulananToolStripMenuItem.Enabled = False
            TahunanToolStripMenuItem.Enabled = False
        ElseIf Label5.Text = 5 Then
            GantiPasswordToolStripMenuItem.Enabled = True
            ToolStripMenuItem1.Enabled = False
            LogOutToolStripMenuItem.Enabled = False
            KeluarToolStripMenuItem.Enabled = False
            TambahKeretaToolStripMenuItem.Enabled = False
            TambahStasiunToolStripMenuItem.Enabled = False
            TambahJadwalToolStripMenuItem.Enabled = False
            DataKetersediaanToolStripMenuItem.Enabled = False
            DataPenumpangToolStripMenuItem.Enabled = False
            TransaksiToolStripMenuItem.Enabled = False
            HarianToolStripMenuItem.Enabled = True
            BulananToolStripMenuItem.Enabled = True
            TahunanToolStripMenuItem.Enabled = True

        End If


    End Sub

    Private Sub GantiPasswordToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GantiPasswordToolStripMenuItem.Click
        FormGantiPassword.Show()
    End Sub

    Private Sub TambahKeretaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahKeretaToolStripMenuItem.Click
        FormTambahKereta.Show()
        Me.Dispose()
    End Sub

    Private Sub TambahStasiunToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahStasiunToolStripMenuItem.Click
        FormTambahStasiun.Show()
        Me.Dispose()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        FormTambahUser.Show()
        Me.Dispose()

    End Sub

    Private Sub TambahJadwalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahJadwalToolStripMenuItem.Click
        FormTambahJadwal.Show()
        Me.Dispose()
    End Sub

    Private Sub DataPenumpangToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DataPenumpangToolStripMenuItem.Click
        FormTambahPenumpang.Show()
        Me.Close()
    End Sub

    Private Sub TransaksiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TransaksiToolStripMenuItem.Click
        FormTransaksi.Show()
        Me.Dispose()

    End Sub

    Private Sub BulananToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BulananToolStripMenuItem.Click
        FormLaporan.Show()
    End Sub

    Private Sub LogOutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LogOutToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan Log Out?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            FormLogin.Show()
            Me.Dispose()
        End If
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KeluarToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan keluar?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            Me.Close()
        End If
    End Sub

    Private Sub DataKetersediaanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DataKetersediaanToolStripMenuItem.Click
        FormKetersediaan.Show()
    End Sub
End Class