﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FormTambahKereta
    Dim databaru As Boolean
    Private Sub FormTambahKereta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bukaDB()
        isigrid1()
        isigrid2()
        isigrid3()

    End Sub
    Sub simpan()
        bukaDB()
        CMD = New SqlCommand("select count(*) from [kereta] where nama_kereta='" & TextBox1.Text & "'", konek)
        Dim DR = CMD.ExecuteScalar

        If (TextBox1.Text = "") Then
            MsgBox("Nama Kereta harus diisi!!", vbCritical, "INFO")
        ElseIf (TextBox2.Text = "" And TextBox3.Text = "" And TextBox4.Text = "") Then
            MsgBox("Harga Harus diisi!!", vbCritical, "INFO")
        ElseIf (NumericUpDown1.Value = 0 And NumericUpDown2.Value = 0 And NumericUpDown3.Value = 0) Then
            MsgBox("Minimal ada 1 gerbong terisi!!", vbCritical, "INFO")
        Else
            Dim CMD1 = New SqlCommand("select count(*) from [kereta] where nama_kereta='" & TextBox1.Text & "'", konek)
            Dim DR1 = CMD1.ExecuteScalar

            If (DR1 = "0") Then
                Dim CMD2 = New SqlCommand("INSERT INTO [kereta](nama_kereta,jumlah_gerbong) values('" & TextBox1.Text & "','" & (CInt(NumericUpDown1.Value) + CInt(NumericUpDown2.Value) + CInt(NumericUpDown3.Value)) & "')", konek)
                Dim DR2 = CMD2.ExecuteNonQuery

                Dim CMD3 = New SqlCommand("select top 1 id_kereta from [kereta] order by id_kereta desc", konek)
                Dim DR3 = CMD3.ExecuteScalar

                Dim gerbong As Integer = 1
                Do While gerbong <= CInt(NumericUpDown1.Value)
                    Dim CMD4 = New SqlCommand("INSERT INTO [gerbong](id_kereta,no_gerbong,kelas,jumlah_kursi,harga) values('" & DR3 & "','" & gerbong & "','Ekonomi','" & CInt(NumericUpDown4.Value) & "','" & TextBox2.Text & "')", konek)
                    Dim DR4 = CMD4.ExecuteNonQuery

                    gerbong += 1

                    Dim CMD10 = New SqlCommand("select * from [gerbong] order by id_gerbong desc", konek)
                    Dim DR10 = CMD10.ExecuteScalar

                    Dim kursi As Integer = 1
                    Do While kursi <= CInt(NumericUpDown4.Value)
                        Dim CMD5 = New SqlCommand("INSERT INTO [kursi](id_kereta,id_gerbong,no_kursi,status_kursi) values('" & DR3 & "','" & DR10 & "','" & kursi & "',0)", konek)
                        Dim DR5 = CMD5.ExecuteNonQuery

                        kursi += 1
                    Loop
                Loop

                Do While gerbong <= CInt(NumericUpDown1.Value) + CInt(NumericUpDown2.Value)
                    Dim CMD6 = New SqlCommand("INSERT INTO [gerbong](id_kereta,no_gerbong,kelas,jumlah_kursi,harga) values('" & DR3 & "','" & gerbong & "','Bisnis','" & CInt(NumericUpDown5.Value) & "','" & TextBox3.Text & "')", konek)
                    Dim DR6 = CMD6.ExecuteNonQuery

                    gerbong += 1

                    Dim CMD11 = New SqlCommand("select * from [gerbong] order by id_gerbong desc", konek)
                    Dim DR11 = CMD11.ExecuteScalar

                    Dim kursi As Integer = 1
                    Do While kursi <= CInt(NumericUpDown5.Value)
                        Dim CMD7 = New SqlCommand("INSERT INTO [kursi](id_kereta,id_gerbong,no_kursi,status_kursi) values('" & DR3 & "','" & DR11 & "','" & kursi & "',0)", konek)
                        Dim DR7 = CMD7.ExecuteNonQuery

                        kursi += 1
                    Loop
                Loop

                Do While gerbong <= CInt(NumericUpDown1.Value) + CInt(NumericUpDown2.Value) + CInt(NumericUpDown3.Value)
                    Dim CMD8 = New SqlCommand("INSERT INTO [gerbong](id_kereta,no_gerbong,kelas,jumlah_kursi,harga) values('" & DR3 & "','" & gerbong & "','Eksekutif','" & CInt(NumericUpDown6.Value) & "','" & TextBox4.Text & "')", konek)
                    Dim DR8 = CMD8.ExecuteNonQuery

                    gerbong += 1

                    Dim CMD12 = New SqlCommand("select * from [gerbong] order by id_gerbong desc", konek)
                    Dim DR12 = CMD12.ExecuteScalar

                    Dim kursi As Integer = 1
                    Do While kursi <= CInt(NumericUpDown6.Value)
                        Dim CMD9 = New SqlCommand("INSERT INTO [kursi](id_kereta,id_gerbong,no_kursi,status_kursi) values('" & DR3 & "','" & DR12 & "','" & kursi & "',0)", konek)
                        Dim DR9 = CMD9.ExecuteNonQuery

                        kursi += 1
                    Loop
                Loop
            End If
        End If
    End Sub

    Sub isigrid1()
        bukaDB()
        DA = New SqlDataAdapter("select * from [kereta]", konek)
        DS = New DataSet
        DS.Clear()
        DA.Fill(DS, "kereta")
        DataGridView1.DataSource = (DS.Tables("kereta"))
        DataGridView1.Enabled = True
    End Sub
    Sub isigrid2()
        bukaDB()
        DA = New SqlDataAdapter("select * from [gerbong]", konek)
        DS = New DataSet
        DS.Clear()
        DA.Fill(DS, "gerbong")
        DataGridView2.DataSource = (DS.Tables("gerbong"))
        DataGridView2.Enabled = True
    End Sub
    Sub isigrid3()
        bukaDB()
        DA = New SqlDataAdapter("select * from [kursi]", konek)
        DS = New DataSet
        DS.Clear()
        DA.Fill(DS, "kursi")
        DataGridView3.DataSource = (DS.Tables("kursi"))
        DataGridView3.Enabled = True
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        databaru = True
        Dim pesan As String

        If TextBox2.Text = "" And TextBox3.Text = "" Then Exit Sub

        If databaru Then
            pesan = MsgBox("Apakah anda yakin akan menambah data ke database??", vbYesNo + vbQuestion)
            If pesan = vbYesNo Then
                Exit Sub
            End If
            simpan()
        Else
            simpan()
            MsgBox("Data Berhasil di simpan!!")

        End If

        DataGridView1.Refresh()
        DataGridView2.Refresh()
        DataGridView3.Refresh()
        isigrid1()
        isigrid2()
        isigrid3()
    End Sub

    Private Sub GantiPasswordToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GantiPasswordToolStripMenuItem.Click
        FormGantiPassword.Show()
    End Sub

    Private Sub TambahKeretaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahKeretaToolStripMenuItem.Click
        Me.Show()
    End Sub

    Private Sub TambahStasiunToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahStasiunToolStripMenuItem.Click
        FormTambahStasiun.Show()
        Me.Dispose()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        FormTambahUser.Show()
        Me.Dispose()

    End Sub

    Private Sub TambahJadwalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahJadwalToolStripMenuItem.Click
        FormTambahJadwal.Show()
        Me.Dispose()
    End Sub

    Private Sub DataPenumpangToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DataPenumpangToolStripMenuItem.Click
        FormTambahPenumpang.Show()
        Me.Close()
    End Sub

    Private Sub TransaksiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TransaksiToolStripMenuItem.Click
        FormTransaksi.Show()
        Me.Dispose()
    End Sub

    Private Sub TextBox2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox2.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then MsgBox("Input Harus Angka")
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub TextBox3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox3.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then MsgBox("Input Harus Angka")
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub TextBox4_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox4.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then MsgBox("Input Harus Angka")
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub BulananToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BulananToolStripMenuItem.Click
        FormLaporan.Show()
    End Sub

    Private Sub LogOutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LogOutToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan Log Out?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            FormLogin.Show()
            Me.Dispose()
        End If
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KeluarToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan keluar?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            Me.Close()
        End If
    End Sub
End Class