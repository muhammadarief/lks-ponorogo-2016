USE [master]
GO
/****** Object:  Database [E_ticketing]    Script Date: 9/12/2016 12:10:37 AM ******/
CREATE DATABASE [E_ticketing]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'E_ticketing', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\E_ticketing.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'E_ticketing_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\E_ticketing_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [E_ticketing] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [E_ticketing].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [E_ticketing] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [E_ticketing] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [E_ticketing] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [E_ticketing] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [E_ticketing] SET ARITHABORT OFF 
GO
ALTER DATABASE [E_ticketing] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [E_ticketing] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [E_ticketing] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [E_ticketing] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [E_ticketing] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [E_ticketing] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [E_ticketing] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [E_ticketing] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [E_ticketing] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [E_ticketing] SET  DISABLE_BROKER 
GO
ALTER DATABASE [E_ticketing] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [E_ticketing] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [E_ticketing] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [E_ticketing] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [E_ticketing] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [E_ticketing] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [E_ticketing] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [E_ticketing] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [E_ticketing] SET  MULTI_USER 
GO
ALTER DATABASE [E_ticketing] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [E_ticketing] SET DB_CHAINING OFF 
GO
ALTER DATABASE [E_ticketing] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [E_ticketing] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [E_ticketing] SET DELAYED_DURABILITY = DISABLED 
GO
USE [E_ticketing]
GO
/****** Object:  Table [dbo].[GB]    Script Date: 9/12/2016 12:10:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GB](
	[ID_GB] [int] NOT NULL,
	[No_gb] [int] NULL,
	[JML_K] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Gerbong]    Script Date: 9/12/2016 12:10:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Gerbong](
	[ID_gerbong] [int] IDENTITY(1,1) NOT NULL,
	[No_gerbong] [int] NULL,
	[Jml_kursi] [int] NULL,
 CONSTRAINT [PK_Gerbong] PRIMARY KEY CLUSTERED 
(
	[ID_gerbong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Jadwal]    Script Date: 9/12/2016 12:10:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Jadwal](
	[ID_jadwal] [int] IDENTITY(1,1) NOT NULL,
	[ID_rute] [int] NOT NULL,
	[tanggal_berangkat] [date] NOT NULL,
	[Waktu_berangkat] [varchar](8) NOT NULL,
	[Tanggal_sampai] [date] NOT NULL,
	[Waktu_sampai] [varchar](8) NOT NULL,
	[Urutan_stasiun_Pemberhentian] [text] NOT NULL,
 CONSTRAINT [PK_Jadwal] PRIMARY KEY CLUSTERED 
(
	[ID_jadwal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KA]    Script Date: 9/12/2016 12:10:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KA](
	[ID_KA] [int] IDENTITY(1,1) NOT NULL,
	[Nama_KA] [varchar](50) NOT NULL,
	[Kelas] [varchar](50) NOT NULL,
	[ID_gerbong] [int] NOT NULL,
 CONSTRAINT [PK_KA] PRIMARY KEY CLUSTERED 
(
	[ID_KA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Petugas]    Script Date: 9/12/2016 12:10:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Petugas](
	[Username] [varchar](50) NOT NULL,
	[Pass] [varchar](50) NOT NULL,
	[Bag] [varchar](50) NOT NULL,
	[No_telp] [bigint] NOT NULL,
	[JK] [varchar](1) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Rute]    Script Date: 9/12/2016 12:10:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rute](
	[ID_rute] [int] IDENTITY(1,1) NOT NULL,
	[Rute_asal] [varchar](50) NOT NULL,
	[Tujuan] [varchar](50) NOT NULL,
	[harga] [numeric](8, 2) NOT NULL,
	[ID_kereta] [int] NOT NULL,
	[USP] [nchar](10) NULL,
 CONSTRAINT [PK_Rute] PRIMARY KEY CLUSTERED 
(
	[ID_rute] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Transaksi]    Script Date: 9/12/2016 12:10:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transaksi](
	[ID_transaksi] [int] IDENTITY(1,1) NOT NULL,
	[ID_jadwal] [int] NOT NULL,
	[Nama_lengkap] [text] NOT NULL,
	[No_id] [bigint] NOT NULL,
	[Alamat] [text] NOT NULL,
	[Jenis_Kelamin] [varchar](1) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[No_telp] [bigint] NOT NULL,
	[No_kursi] [int] NOT NULL,
	[Harga] [numeric](8, 2) NOT NULL,
	[Tanggal_transaksi] [date] NOT NULL,
	[Boarding] [varchar](15) NULL,
 CONSTRAINT [PK_Transaksi] PRIMARY KEY CLUSTERED 
(
	[ID_transaksi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Gerbong] ON 

INSERT [dbo].[Gerbong] ([ID_gerbong], [No_gerbong], [Jml_kursi]) VALUES (1, 1, 10)
INSERT [dbo].[Gerbong] ([ID_gerbong], [No_gerbong], [Jml_kursi]) VALUES (2, 2, 10)
INSERT [dbo].[Gerbong] ([ID_gerbong], [No_gerbong], [Jml_kursi]) VALUES (3, 3, 10)
INSERT [dbo].[Gerbong] ([ID_gerbong], [No_gerbong], [Jml_kursi]) VALUES (4, 4, 10)
INSERT [dbo].[Gerbong] ([ID_gerbong], [No_gerbong], [Jml_kursi]) VALUES (5, 5, 10)
SET IDENTITY_INSERT [dbo].[Gerbong] OFF
SET IDENTITY_INSERT [dbo].[Jadwal] ON 

INSERT [dbo].[Jadwal] ([ID_jadwal], [ID_rute], [tanggal_berangkat], [Waktu_berangkat], [Tanggal_sampai], [Waktu_sampai], [Urutan_stasiun_Pemberhentian]) VALUES (1, 2, CAST(N'2016-09-10' AS Date), N'12:00:00', CAST(N'2016-08-10' AS Date), N'05:00:00', N'saya-bingung-mas')
INSERT [dbo].[Jadwal] ([ID_jadwal], [ID_rute], [tanggal_berangkat], [Waktu_berangkat], [Tanggal_sampai], [Waktu_sampai], [Urutan_stasiun_Pemberhentian]) VALUES (2, 1, CAST(N'2016-09-10' AS Date), N'12:00:00', CAST(N'2016-08-10' AS Date), N'05:00:00', N'iswahyudi-juanda-ngurah rai')
INSERT [dbo].[Jadwal] ([ID_jadwal], [ID_rute], [tanggal_berangkat], [Waktu_berangkat], [Tanggal_sampai], [Waktu_sampai], [Urutan_stasiun_Pemberhentian]) VALUES (3, 3, CAST(N'2016-09-10' AS Date), N'12:00:00', CAST(N'2016-08-10' AS Date), N'05:00:00', N'iswahyudi-king abdul aziz-malioboro')
INSERT [dbo].[Jadwal] ([ID_jadwal], [ID_rute], [tanggal_berangkat], [Waktu_berangkat], [Tanggal_sampai], [Waktu_sampai], [Urutan_stasiun_Pemberhentian]) VALUES (7, 6, CAST(N'2016-09-10' AS Date), N'12:00:00', CAST(N'2016-08-10' AS Date), N'05:00:00', N'lupa-nama-stasiunnya')
INSERT [dbo].[Jadwal] ([ID_jadwal], [ID_rute], [tanggal_berangkat], [Waktu_berangkat], [Tanggal_sampai], [Waktu_sampai], [Urutan_stasiun_Pemberhentian]) VALUES (8, 7, CAST(N'2016-09-10' AS Date), N'12:00:00', CAST(N'2016-08-10' AS Date), N'05:00:00', N'diisi-setelah-nanti')
INSERT [dbo].[Jadwal] ([ID_jadwal], [ID_rute], [tanggal_berangkat], [Waktu_berangkat], [Tanggal_sampai], [Waktu_sampai], [Urutan_stasiun_Pemberhentian]) VALUES (9, 2, CAST(N'2016-09-11' AS Date), N'4:28', CAST(N'2016-09-07' AS Date), N'9:8', N'tes-tes-tes')
INSERT [dbo].[Jadwal] ([ID_jadwal], [ID_rute], [tanggal_berangkat], [Waktu_berangkat], [Tanggal_sampai], [Waktu_sampai], [Urutan_stasiun_Pemberhentian]) VALUES (10, 3, CAST(N'2016-09-09' AS Date), N'5:8', CAST(N'2016-09-13' AS Date), N'11:5', N'ponorogo-jakarta-madiun')
SET IDENTITY_INSERT [dbo].[Jadwal] OFF
SET IDENTITY_INSERT [dbo].[KA] ON 

INSERT [dbo].[KA] ([ID_KA], [Nama_KA], [Kelas], [ID_gerbong]) VALUES (1, N'Argowilis', N'Eksekutif', 1)
INSERT [dbo].[KA] ([ID_KA], [Nama_KA], [Kelas], [ID_gerbong]) VALUES (2, N'Argowilis', N'Bisnis', 2)
INSERT [dbo].[KA] ([ID_KA], [Nama_KA], [Kelas], [ID_gerbong]) VALUES (3, N'Argobromo', N'Bisnis', 3)
INSERT [dbo].[KA] ([ID_KA], [Nama_KA], [Kelas], [ID_gerbong]) VALUES (4, N'Argobromo', N'eksekutif', 3)
INSERT [dbo].[KA] ([ID_KA], [Nama_KA], [Kelas], [ID_gerbong]) VALUES (5, N'Argobromo', N'ekonomi', 4)
INSERT [dbo].[KA] ([ID_KA], [Nama_KA], [Kelas], [ID_gerbong]) VALUES (6, N'Argowilis', N'Eksekutif', 1)
SET IDENTITY_INSERT [dbo].[KA] OFF
INSERT [dbo].[Petugas] ([Username], [Pass], [Bag], [No_telp], [JK]) VALUES (N'Admin', N'Admin', N'Administrator', 1, N'L')
INSERT [dbo].[Petugas] ([Username], [Pass], [Bag], [No_telp], [JK]) VALUES (N'Boarding', N'board', N'Boarding', 1, N'L')
INSERT [dbo].[Petugas] ([Username], [Pass], [Bag], [No_telp], [JK]) VALUES (N'Laket', N'tiket', N'Layanan Tiket', 1, N'L')
INSERT [dbo].[Petugas] ([Username], [Pass], [Bag], [No_telp], [JK]) VALUES (N'Operator', N'operator', N'Operator', 1, N'L')
INSERT [dbo].[Petugas] ([Username], [Pass], [Bag], [No_telp], [JK]) VALUES (N'Penjadwalan', N'Jadwal', N'Penjadwalan', 1, N'L')
INSERT [dbo].[Petugas] ([Username], [Pass], [Bag], [No_telp], [JK]) VALUES (N'shameda', N'hyukaze', N'Boarding', 123, N'L')
INSERT [dbo].[Petugas] ([Username], [Pass], [Bag], [No_telp], [JK]) VALUES (N'shgdsa', N'kjsdgbf', N'Penjadwalan', 232142, N'L')
SET IDENTITY_INSERT [dbo].[Rute] ON 

INSERT [dbo].[Rute] ([ID_rute], [Rute_asal], [Tujuan], [harga], [ID_kereta], [USP]) VALUES (1, N'ponorogo', N'jakarta', CAST(200000.00 AS Numeric(8, 2)), 5, NULL)
INSERT [dbo].[Rute] ([ID_rute], [Rute_asal], [Tujuan], [harga], [ID_kereta], [USP]) VALUES (2, N'ponorogo', N'jenangan', CAST(123424.00 AS Numeric(8, 2)), 4, NULL)
INSERT [dbo].[Rute] ([ID_rute], [Rute_asal], [Tujuan], [harga], [ID_kereta], [USP]) VALUES (3, N'ponorogo', N'Arab', CAST(123424.00 AS Numeric(8, 2)), 6, NULL)
INSERT [dbo].[Rute] ([ID_rute], [Rute_asal], [Tujuan], [harga], [ID_kereta], [USP]) VALUES (6, N'ponorogo', N'Nusakambangan', CAST(120000.00 AS Numeric(8, 2)), 3, NULL)
INSERT [dbo].[Rute] ([ID_rute], [Rute_asal], [Tujuan], [harga], [ID_kereta], [USP]) VALUES (7, N'ponorogo', N'gemaharjo', CAST(20000.00 AS Numeric(8, 2)), 3, NULL)
INSERT [dbo].[Rute] ([ID_rute], [Rute_asal], [Tujuan], [harga], [ID_kereta], [USP]) VALUES (9, N'ponorogo', N'Pacitan', CAST(123424.00 AS Numeric(8, 2)), 5, NULL)
INSERT [dbo].[Rute] ([ID_rute], [Rute_asal], [Tujuan], [harga], [ID_kereta], [USP]) VALUES (13, N'ponorogo', N'malaysia', CAST(123424.00 AS Numeric(8, 2)), 2, NULL)
INSERT [dbo].[Rute] ([ID_rute], [Rute_asal], [Tujuan], [harga], [ID_kereta], [USP]) VALUES (14, N'ponorogo', N'malaysia', CAST(123424.00 AS Numeric(8, 2)), 2, NULL)
INSERT [dbo].[Rute] ([ID_rute], [Rute_asal], [Tujuan], [harga], [ID_kereta], [USP]) VALUES (15, N'ponorogo', N'malaysia', CAST(123424.00 AS Numeric(8, 2)), 2, NULL)
SET IDENTITY_INSERT [dbo].[Rute] OFF
ALTER TABLE [dbo].[Jadwal]  WITH CHECK ADD  CONSTRAINT [FK_ID_RUTE_RUTE] FOREIGN KEY([ID_rute])
REFERENCES [dbo].[Rute] ([ID_rute])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Jadwal] CHECK CONSTRAINT [FK_ID_RUTE_RUTE]
GO
USE [master]
GO
ALTER DATABASE [E_ticketing] SET  READ_WRITE 
GO
