﻿Imports System.Data.SqlClient
Public Class Admin

    Private Sub Admin_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Button5.PerformClick()

    End Sub

    Private Sub Admin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        BK()
        lipeng()
        DataGridView1.DataSource = st.Tables("petugas")
        TextBox2.Select()
        Label1.Text = Login.us
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If TextBox1.Text <> "" And TextBox2.Text <> "" And TextBox3.Text <> "" And ComboBox1.Text <> "" And DataGridView1.RowCount.ToString > 2 And RadioButton1.Checked = True Or RadioButton2.Checked = True Then
            Dim G As String
            If RadioButton1.Checked = True Then
                G = RadioButton1.Text
            ElseIf RadioButton2.Checked = True Then
                G = RadioButton2.Text
            End If
            Dim ix = "insert into petugas values('" & TextBox2.Text & "','" & TextBox1.Text & "','" & ComboBox1.SelectedItem & "','" & TextBox3.Text & "','" & G & "')"
            Try
                md = New SqlCommand(ix, con)
                md.ExecuteNonQuery()
                MsgBox("Berhasil")
                lipeng()
                DataGridView1.DataSource = st.Tables("petugas")
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MsgBox("semua data harus diisi")
        End If
    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged
        red.Close()
        Dim da As String = "select * from petugas where username='" & TextBox2.Text & "'"
        md = New SqlCommand(da, con)
        red = md.ExecuteReader
        Try
            red.Read()
            If red.HasRows = True Then
                TextBox1.Text = red.Item("pass")
                TextBox3.Text = red.Item("no_telp")

                If red.Item("JK") = "L" Then
                    RadioButton1.Checked = True
                ElseIf red.Item("JK") = "P" Then
                    RadioButton2.Checked = True
                End If
                'ComboBox1.ValueMember = red.Item("bagian")
                red.Close()
                dapt = New SqlDataAdapter("select * from petugas where username='" & TextBox2.Text & "'", con)
                st = New DataSet
                dapt.Fill(st, "Cargas")
                DataGridView1.DataSource = st.Tables("cargas")
            Else
                TextBox1.Text = ""
                TextBox3.Text = ""
                RadioButton1.Checked = False
                RadioButton2.Checked = False
                red.Close()
                lipeng()
                DataGridView1.DataSource = st.Tables("petugas")
            End If
        Catch ex As Exception
            DataGridView1.DataSource = st.Tables("petugas")
            MsgBox("error")
            MsgBox(ex.Message)
        End Try
        If TextBox2.Text <> "" And TextBox2.Text <> "" And TextBox3.Text <> "" And DataGridView1.RowCount = 2 Then
            Button1.Enabled = False
        Else
            Button1.Enabled = True
        End If

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If MsgBox("apakah anda ingin keluar", vbYesNo, "PONOROGO EXPRESS") = vbYes Then
            Me.Hide()
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If TextBox2.Text <> "" And TextBox1.Text <> "" And TextBox3.Text <> "" Then
            Try
                Dim qu As String = "delete from petugas where username='" & TextBox2.Text & "'"
                md = New SqlCommand(qu, con)
                md.ExecuteNonQuery()
                lipeng()
                DataGridView1.DataSource = st.Tables("petugas")
                MsgBox("Berhasil menghapus")
                TextBox1.Text = ""
                TextBox2.Text = ""
                TextBox3.Text = ""
            Catch ex As Exception
                MsgBox("Gagal menghapus")
            End Try
        Else
            MsgBox("masukkan data yang ingi dihapus")
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim G As String
        If RadioButton1.Checked = True Then
            G = RadioButton1.Text
        ElseIf RadioButton2.Checked = True Then
            G = RadioButton2.Text
        End If
        If TextBox2.Text <> "" And TextBox1.Text <> "" And TextBox3.Text <> "" And ComboBox1.Text <> "" And RadioButton1.Checked = True Or RadioButton2.Checked = True Then
            Try
                Dim qu As String = "update petugas set pass='" & TextBox2.Text & "' bag='" & ComboBox1.Text & "' no_telp='" & TextBox3.Text & "' JK='" & G & "'"
                md = New SqlCommand(qu, con)
                md.ExecuteNonQuery()
                lipeng()
                DataGridView1.DataSource = st.Tables("petugas")
                MsgBox("Berhasil menghapus")
                TextBox1.Text = ""
                TextBox2.Text = ""
                TextBox3.Text = ""
            Catch ex As Exception
                MsgBox("Gagal menghapus")
            End Try
        Else
            MsgBox("masukkan data yang ingi dihapus")
        End If
    End Sub

    Private Sub KeretaApiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KeretaApiToolStripMenuItem.Click
        KA.ShowDialog()
    End Sub

    Private Sub TextBox3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox3.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox3_TextChanged(sender As Object, e As EventArgs) Handles TextBox3.TextChanged

    End Sub

    Private Sub EditDataDiriToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EditDataDiriToolStripMenuItem.Click
        Data_Diri.ShowDialog()
    End Sub

    Private Sub PenjadwalanKeretaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PenjadwalanKeretaToolStripMenuItem.Click
        PEnjadwalan.ShowDialog()
    End Sub

    Private Sub TransaksiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TransaksiToolStripMenuItem.Click
        layanan_tiket.ShowDialog()
    End Sub

    Private Sub PenjadwalanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PenjadwalanToolStripMenuItem.Click
        PEnjadwalan.ShowDialog()
    End Sub

    Private Sub RuteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RuteToolStripMenuItem.Click
        Rute.ShowDialog()
    End Sub
End Class