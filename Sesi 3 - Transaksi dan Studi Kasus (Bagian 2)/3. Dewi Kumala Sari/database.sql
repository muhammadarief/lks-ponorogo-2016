USE [master]
GO
/****** Object:  Database [db_kereta]    Script Date: 9/12/2016 4:27:24 AM ******/
CREATE DATABASE [db_kereta]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_kereta', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.DEWI\MSSQL\DATA\db_kereta.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'db_kereta_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.DEWI\MSSQL\DATA\db_kereta_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [db_kereta] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_kereta].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_kereta] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_kereta] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_kereta] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_kereta] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_kereta] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_kereta] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_kereta] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_kereta] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_kereta] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_kereta] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_kereta] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_kereta] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_kereta] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_kereta] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_kereta] SET  DISABLE_BROKER 
GO
ALTER DATABASE [db_kereta] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_kereta] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_kereta] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_kereta] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_kereta] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_kereta] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_kereta] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_kereta] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [db_kereta] SET  MULTI_USER 
GO
ALTER DATABASE [db_kereta] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_kereta] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_kereta] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_kereta] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [db_kereta] SET DELAYED_DURABILITY = DISABLED 
GO
USE [db_kereta]
GO
/****** Object:  Table [dbo].[akses]    Script Date: 9/12/2016 4:27:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[akses](
	[id_akses] [int] NOT NULL,
	[status] [varchar](50) NULL,
 CONSTRAINT [PK_akses] PRIMARY KEY CLUSTERED 
(
	[id_akses] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gerbong]    Script Date: 9/12/2016 4:27:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gerbong](
	[id_gerbong] [int] IDENTITY(1,1) NOT NULL,
	[id_kereta] [int] NULL,
	[no_gerbong] [int] NULL,
	[kelas] [varchar](30) NULL,
	[jumlah_kursi] [int] NULL,
	[harga] [float] NULL,
 CONSTRAINT [PK_gerbong] PRIMARY KEY CLUSTERED 
(
	[id_gerbong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[jadwal]    Script Date: 9/12/2016 4:27:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[jadwal](
	[id_jadwal] [varchar](20) NOT NULL,
	[id_kereta] [int] NULL,
	[stasiun_awal] [varchar](50) NULL,
	[stasiun_tujuan] [varchar](50) NULL,
	[stasiun_berhenti] [varchar](50) NULL,
	[waktu_berangkat] [datetime] NULL,
	[waktu_datang] [datetime] NULL,
	[kursi_ekonomi] [int] NULL,
	[kursi_bisnis] [int] NULL,
	[kursi_eksekutif] [int] NULL,
	[harga_ekonomi] [float] NULL,
	[harga_bisnis] [float] NOT NULL,
	[harga_eksekutif] [float] NULL,
	[status] [varchar](20) NULL,
 CONSTRAINT [PK_jadwal] PRIMARY KEY CLUSTERED 
(
	[id_jadwal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[kereta]    Script Date: 9/12/2016 4:27:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[kereta](
	[id_kereta] [int] IDENTITY(1,1) NOT NULL,
	[nama_kereta] [varchar](50) NULL,
	[jumlah_gerbong] [int] NULL,
 CONSTRAINT [PK_kereta] PRIMARY KEY CLUSTERED 
(
	[id_kereta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[kursi]    Script Date: 9/12/2016 4:27:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[kursi](
	[id_kursi] [int] IDENTITY(1,1) NOT NULL,
	[id_kereta] [int] NULL,
	[id_gerbong] [int] NULL,
	[no_kursi] [int] NULL,
	[status_kursi] [int] NULL,
 CONSTRAINT [PK_kursi] PRIMARY KEY CLUSTERED 
(
	[id_kursi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[pegawai]    Script Date: 9/12/2016 4:27:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pegawai](
	[id_pegawai] [varchar](20) NOT NULL,
	[nama_pegawai] [varchar](20) NULL,
	[tempat_lahir] [varchar](25) NULL,
	[tanggal_lahir] [date] NULL,
	[alamat] [varchar](75) NULL,
	[no_tlp] [varchar](20) NULL,
	[email] [varchar](50) NULL,
	[stasiun_bekerja] [varchar](50) NULL,
 CONSTRAINT [PK_pegawai] PRIMARY KEY CLUSTERED 
(
	[id_pegawai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[penumpang]    Script Date: 9/12/2016 4:27:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[penumpang](
	[id_penumpang] [varchar](20) NOT NULL,
	[nama_penumpang] [varchar](50) NULL,
	[no_identitas] [varchar](50) NULL,
	[alamat] [varchar](50) NULL,
	[jenis_kelamin] [varchar](20) NULL,
	[email] [varchar](50) NULL,
	[no_tlp] [varchar](25) NULL,
 CONSTRAINT [PK_penumpang] PRIMARY KEY CLUSTERED 
(
	[id_penumpang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[stasiun]    Script Date: 9/12/2016 4:27:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[stasiun](
	[id_stasiun] [varchar](20) NOT NULL,
	[nama_stasiun] [varchar](50) NULL,
	[kota] [varchar](50) NULL,
	[provinsi] [varchar](50) NULL,
 CONSTRAINT [PK_stasiun] PRIMARY KEY CLUSTERED 
(
	[id_stasiun] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[transaksi]    Script Date: 9/12/2016 4:27:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transaksi](
	[id_transaksi] [varchar](20) NOT NULL,
	[id_penumpang] [varchar](20) NULL,
	[id_user] [varchar](20) NULL,
	[id_kereta] [int] NULL,
	[stasiun_awal] [varchar](50) NULL,
	[stasiun_berhenti] [varchar](50) NULL,
	[waktu_berangkat] [varchar](50) NULL,
	[waktu_datang] [varchar](50) NULL,
	[kelas] [varchar](20) NULL,
	[no_gerbong] [int] NULL,
	[no_kursi] [int] NULL,
	[harga] [float] NULL,
	[tgl_beli] [date] NULL,
 CONSTRAINT [PK_transaksi] PRIMARY KEY CLUSTERED 
(
	[id_transaksi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user]    Script Date: 9/12/2016 4:27:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user](
	[id_user] [varchar](20) NOT NULL,
	[id_pegawai] [varchar](20) NULL,
	[id_akses] [int] NULL,
	[username] [varchar](50) NULL,
	[password] [varchar](50) NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[id_user] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[v_akun]    Script Date: 9/12/2016 4:27:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_akun]
AS
SELECT        dbo.pegawai.id_pegawai, dbo.pegawai.nama_pegawai, dbo.pegawai.tempat_lahir, dbo.pegawai.tanggal_lahir, dbo.pegawai.alamat, dbo.pegawai.no_tlp, 
                         dbo.pegawai.email, dbo.pegawai.stasiun_bekerja, dbo.akses.status, dbo.[user].username, dbo.[user].password
FROM            dbo.akses INNER JOIN
                         dbo.[user] ON dbo.akses.id_akses = dbo.[user].id_akses INNER JOIN
                         dbo.pegawai ON dbo.[user].id_pegawai = dbo.pegawai.id_pegawai

GO
INSERT [dbo].[akses] ([id_akses], [status]) VALUES (1, N'Admin')
INSERT [dbo].[akses] ([id_akses], [status]) VALUES (2, N'Penjadwalan')
INSERT [dbo].[akses] ([id_akses], [status]) VALUES (3, N'Tiketing')
INSERT [dbo].[akses] ([id_akses], [status]) VALUES (4, N'Boarding')
INSERT [dbo].[akses] ([id_akses], [status]) VALUES (5, N'Operasi')
SET IDENTITY_INSERT [dbo].[gerbong] ON 

INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (1, 1, 1, N'Ekonomi', 25, 100000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (2, 1, 2, N'Ekonomi', 25, 100000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (3, 1, 3, N'Ekonomi', 25, 100000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (4, 1, 4, N'Ekonomi', 25, 100000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (5, 1, 5, N'Ekonomi', 25, 100000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (6, 1, 6, N'Ekonomi', 25, 100000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (7, 1, 7, N'Ekonomi', 25, 100000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (8, 1, 8, N'Ekonomi', 25, 100000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (9, 1, 9, N'Ekonomi', 25, 100000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (10, 1, 10, N'Ekonomi', 25, 100000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (11, 1, 11, N'Bisnis', 20, 150000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (12, 1, 12, N'Bisnis', 20, 150000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (13, 1, 13, N'Bisnis', 20, 150000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (14, 1, 14, N'Bisnis', 20, 150000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (15, 1, 15, N'Bisnis', 20, 150000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (16, 1, 16, N'Bisnis', 20, 150000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (17, 1, 17, N'Bisnis', 20, 150000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (18, 1, 18, N'Eksekutif', 12, 200000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (19, 1, 19, N'Eksekutif', 12, 200000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (20, 1, 20, N'Eksekutif', 12, 200000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (21, 1, 21, N'Eksekutif', 12, 200000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (22, 1, 22, N'Eksekutif', 12, 200000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (23, 2, 1, N'Ekonomi', 14, 120000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (24, 2, 2, N'Ekonomi', 14, 120000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (25, 2, 3, N'Ekonomi', 14, 120000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (26, 2, 4, N'Ekonomi', 14, 120000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (27, 2, 5, N'Ekonomi', 14, 120000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (28, 2, 6, N'Ekonomi', 14, 120000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (29, 2, 7, N'Bisnis', 9, 145000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (30, 2, 8, N'Bisnis', 9, 145000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (31, 2, 9, N'Bisnis', 9, 145000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (32, 2, 10, N'Bisnis', 9, 145000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (33, 2, 11, N'Bisnis', 9, 145000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (34, 2, 12, N'Eksekutif', 7, 185000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (35, 2, 13, N'Eksekutif', 7, 185000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (36, 2, 14, N'Eksekutif', 7, 185000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (37, 2, 15, N'Eksekutif', 7, 185000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (38, 3, 1, N'Ekonomi', 20, 100000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (39, 3, 2, N'Ekonomi', 20, 100000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (40, 3, 3, N'Ekonomi', 20, 100000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (41, 3, 4, N'Bisnis', 16, 200000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (42, 3, 5, N'Bisnis', 16, 200000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (43, 3, 6, N'Bisnis', 16, 200000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (44, 3, 7, N'Eksekutif', 15, 300000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (45, 3, 8, N'Eksekutif', 15, 300000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (46, 3, 9, N'Eksekutif', 15, 300000)
SET IDENTITY_INSERT [dbo].[gerbong] OFF
INSERT [dbo].[jadwal] ([id_jadwal], [id_kereta], [stasiun_awal], [stasiun_tujuan], [stasiun_berhenti], [waktu_berangkat], [waktu_datang], [kursi_ekonomi], [kursi_bisnis], [kursi_eksekutif], [harga_ekonomi], [harga_bisnis], [harga_eksekutif], [status]) VALUES (N'J-01', 1, N'Gambir', N'Surabaya Gubeng', N'Wates', CAST(N'2016-09-11 11:00:00.000' AS DateTime), CAST(N'2016-09-11 23:00:00.000' AS DateTime), 250, 140, 60, 100000, 150000, 200000, N'Tidak Aktif')
INSERT [dbo].[jadwal] ([id_jadwal], [id_kereta], [stasiun_awal], [stasiun_tujuan], [stasiun_berhenti], [waktu_berangkat], [waktu_datang], [kursi_ekonomi], [kursi_bisnis], [kursi_eksekutif], [harga_ekonomi], [harga_bisnis], [harga_eksekutif], [status]) VALUES (N'J-02', 1, N'Gambir', N'Surabaya Gubeng', N'Madiun', CAST(N'2016-09-11 11:00:00.000' AS DateTime), CAST(N'2016-09-11 23:00:00.000' AS DateTime), 250, 140, 60, 100000, 150000, 200000, N'Tidak Aktif')
INSERT [dbo].[jadwal] ([id_jadwal], [id_kereta], [stasiun_awal], [stasiun_tujuan], [stasiun_berhenti], [waktu_berangkat], [waktu_datang], [kursi_ekonomi], [kursi_bisnis], [kursi_eksekutif], [harga_ekonomi], [harga_bisnis], [harga_eksekutif], [status]) VALUES (N'J-03', 1, N'Gambir', N'Surabaya Gubeng', N'Malang', CAST(N'2016-09-11 11:00:00.000' AS DateTime), CAST(N'2016-09-11 23:00:00.000' AS DateTime), 250, 140, 60, 100000, 150000, 200000, N'Tidak Aktif')
INSERT [dbo].[jadwal] ([id_jadwal], [id_kereta], [stasiun_awal], [stasiun_tujuan], [stasiun_berhenti], [waktu_berangkat], [waktu_datang], [kursi_ekonomi], [kursi_bisnis], [kursi_eksekutif], [harga_ekonomi], [harga_bisnis], [harga_eksekutif], [status]) VALUES (N'J-04', 1, N'Gambir', N'Surabaya Gubeng', N'Surabaya Kota', CAST(N'2016-09-11 11:00:00.000' AS DateTime), CAST(N'2016-09-11 23:00:00.000' AS DateTime), 250, 140, 60, 100000, 150000, 200000, N'Tidak Aktif')
INSERT [dbo].[jadwal] ([id_jadwal], [id_kereta], [stasiun_awal], [stasiun_tujuan], [stasiun_berhenti], [waktu_berangkat], [waktu_datang], [kursi_ekonomi], [kursi_bisnis], [kursi_eksekutif], [harga_ekonomi], [harga_bisnis], [harga_eksekutif], [status]) VALUES (N'J-05', 3, N'Gambir', N'Malang', N'Madiun', CAST(N'2016-09-12 04:00:31.000' AS DateTime), CAST(N'2016-09-12 04:00:31.000' AS DateTime), 60, 48, 45, 100000, 200000, 300000, N'Aktif')
INSERT [dbo].[jadwal] ([id_jadwal], [id_kereta], [stasiun_awal], [stasiun_tujuan], [stasiun_berhenti], [waktu_berangkat], [waktu_datang], [kursi_ekonomi], [kursi_bisnis], [kursi_eksekutif], [harga_ekonomi], [harga_bisnis], [harga_eksekutif], [status]) VALUES (N'J-06', 3, N'Gambir', N'Malang', N'Surabaya Gubeng', CAST(N'2016-09-12 04:00:31.000' AS DateTime), CAST(N'2016-09-12 04:00:31.000' AS DateTime), 60, 48, 45, 100000, 200000, 300000, N'Aktif')
INSERT [dbo].[jadwal] ([id_jadwal], [id_kereta], [stasiun_awal], [stasiun_tujuan], [stasiun_berhenti], [waktu_berangkat], [waktu_datang], [kursi_ekonomi], [kursi_bisnis], [kursi_eksekutif], [harga_ekonomi], [harga_bisnis], [harga_eksekutif], [status]) VALUES (N'J-07', 3, N'Gambir', N'Malang', N'Surabaya Kota', CAST(N'2016-09-12 04:00:31.000' AS DateTime), CAST(N'2016-09-12 04:00:31.000' AS DateTime), 60, 48, 45, 100000, 200000, 300000, N'Aktif')
SET IDENTITY_INSERT [dbo].[kereta] ON 

INSERT [dbo].[kereta] ([id_kereta], [nama_kereta], [jumlah_gerbong]) VALUES (1, N'Gajayana', 22)
INSERT [dbo].[kereta] ([id_kereta], [nama_kereta], [jumlah_gerbong]) VALUES (2, N'Bima', 15)
INSERT [dbo].[kereta] ([id_kereta], [nama_kereta], [jumlah_gerbong]) VALUES (3, N'LokaJaya', 9)
SET IDENTITY_INSERT [dbo].[kereta] OFF
SET IDENTITY_INSERT [dbo].[kursi] ON 

INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (1, 1, 1, 1, 1)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (2, 1, 1, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (3, 1, 1, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (4, 1, 1, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (5, 1, 1, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (6, 1, 1, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (7, 1, 1, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (8, 1, 1, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (9, 1, 1, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (10, 1, 1, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (11, 1, 1, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (12, 1, 1, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (13, 1, 1, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (14, 1, 1, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (15, 1, 1, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (16, 1, 1, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (17, 1, 1, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (18, 1, 1, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (19, 1, 1, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (20, 1, 1, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (21, 1, 1, 21, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (22, 1, 1, 22, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (23, 1, 1, 23, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (24, 1, 1, 24, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (25, 1, 1, 25, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (26, 1, 2, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (27, 1, 2, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (28, 1, 2, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (29, 1, 2, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (30, 1, 2, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (31, 1, 2, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (32, 1, 2, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (33, 1, 2, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (34, 1, 2, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (35, 1, 2, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (36, 1, 2, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (37, 1, 2, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (38, 1, 2, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (39, 1, 2, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (40, 1, 2, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (41, 1, 2, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (42, 1, 2, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (43, 1, 2, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (44, 1, 2, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (45, 1, 2, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (46, 1, 2, 21, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (47, 1, 2, 22, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (48, 1, 2, 23, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (49, 1, 2, 24, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (50, 1, 2, 25, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (51, 1, 3, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (52, 1, 3, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (53, 1, 3, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (54, 1, 3, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (55, 1, 3, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (56, 1, 3, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (57, 1, 3, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (58, 1, 3, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (59, 1, 3, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (60, 1, 3, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (61, 1, 3, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (62, 1, 3, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (63, 1, 3, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (64, 1, 3, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (65, 1, 3, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (66, 1, 3, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (67, 1, 3, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (68, 1, 3, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (69, 1, 3, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (70, 1, 3, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (71, 1, 3, 21, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (72, 1, 3, 22, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (73, 1, 3, 23, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (74, 1, 3, 24, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (75, 1, 3, 25, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (76, 1, 4, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (77, 1, 4, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (78, 1, 4, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (79, 1, 4, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (80, 1, 4, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (81, 1, 4, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (82, 1, 4, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (83, 1, 4, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (84, 1, 4, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (85, 1, 4, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (86, 1, 4, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (87, 1, 4, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (88, 1, 4, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (89, 1, 4, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (90, 1, 4, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (91, 1, 4, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (92, 1, 4, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (93, 1, 4, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (94, 1, 4, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (95, 1, 4, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (96, 1, 4, 21, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (97, 1, 4, 22, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (98, 1, 4, 23, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (99, 1, 4, 24, 0)
GO
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (100, 1, 4, 25, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (101, 1, 5, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (102, 1, 5, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (103, 1, 5, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (104, 1, 5, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (105, 1, 5, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (106, 1, 5, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (107, 1, 5, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (108, 1, 5, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (109, 1, 5, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (110, 1, 5, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (111, 1, 5, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (112, 1, 5, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (113, 1, 5, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (114, 1, 5, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (115, 1, 5, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (116, 1, 5, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (117, 1, 5, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (118, 1, 5, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (119, 1, 5, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (120, 1, 5, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (121, 1, 5, 21, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (122, 1, 5, 22, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (123, 1, 5, 23, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (124, 1, 5, 24, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (125, 1, 5, 25, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (126, 1, 6, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (127, 1, 6, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (128, 1, 6, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (129, 1, 6, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (130, 1, 6, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (131, 1, 6, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (132, 1, 6, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (133, 1, 6, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (134, 1, 6, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (135, 1, 6, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (136, 1, 6, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (137, 1, 6, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (138, 1, 6, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (139, 1, 6, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (140, 1, 6, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (141, 1, 6, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (142, 1, 6, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (143, 1, 6, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (144, 1, 6, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (145, 1, 6, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (146, 1, 6, 21, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (147, 1, 6, 22, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (148, 1, 6, 23, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (149, 1, 6, 24, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (150, 1, 6, 25, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (151, 1, 7, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (152, 1, 7, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (153, 1, 7, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (154, 1, 7, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (155, 1, 7, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (156, 1, 7, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (157, 1, 7, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (158, 1, 7, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (159, 1, 7, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (160, 1, 7, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (161, 1, 7, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (162, 1, 7, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (163, 1, 7, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (164, 1, 7, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (165, 1, 7, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (166, 1, 7, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (167, 1, 7, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (168, 1, 7, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (169, 1, 7, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (170, 1, 7, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (171, 1, 7, 21, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (172, 1, 7, 22, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (173, 1, 7, 23, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (174, 1, 7, 24, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (175, 1, 7, 25, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (176, 1, 8, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (177, 1, 8, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (178, 1, 8, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (179, 1, 8, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (180, 1, 8, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (181, 1, 8, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (182, 1, 8, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (183, 1, 8, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (184, 1, 8, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (185, 1, 8, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (186, 1, 8, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (187, 1, 8, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (188, 1, 8, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (189, 1, 8, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (190, 1, 8, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (191, 1, 8, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (192, 1, 8, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (193, 1, 8, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (194, 1, 8, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (195, 1, 8, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (196, 1, 8, 21, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (197, 1, 8, 22, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (198, 1, 8, 23, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (199, 1, 8, 24, 0)
GO
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (200, 1, 8, 25, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (201, 1, 9, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (202, 1, 9, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (203, 1, 9, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (204, 1, 9, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (205, 1, 9, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (206, 1, 9, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (207, 1, 9, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (208, 1, 9, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (209, 1, 9, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (210, 1, 9, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (211, 1, 9, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (212, 1, 9, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (213, 1, 9, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (214, 1, 9, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (215, 1, 9, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (216, 1, 9, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (217, 1, 9, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (218, 1, 9, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (219, 1, 9, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (220, 1, 9, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (221, 1, 9, 21, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (222, 1, 9, 22, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (223, 1, 9, 23, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (224, 1, 9, 24, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (225, 1, 9, 25, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (226, 1, 10, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (227, 1, 10, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (228, 1, 10, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (229, 1, 10, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (230, 1, 10, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (231, 1, 10, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (232, 1, 10, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (233, 1, 10, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (234, 1, 10, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (235, 1, 10, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (236, 1, 10, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (237, 1, 10, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (238, 1, 10, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (239, 1, 10, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (240, 1, 10, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (241, 1, 10, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (242, 1, 10, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (243, 1, 10, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (244, 1, 10, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (245, 1, 10, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (246, 1, 10, 21, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (247, 1, 10, 22, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (248, 1, 10, 23, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (249, 1, 10, 24, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (250, 1, 10, 25, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (251, 1, 11, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (252, 1, 11, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (253, 1, 11, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (254, 1, 11, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (255, 1, 11, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (256, 1, 11, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (257, 1, 11, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (258, 1, 11, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (259, 1, 11, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (260, 1, 11, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (261, 1, 11, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (262, 1, 11, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (263, 1, 11, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (264, 1, 11, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (265, 1, 11, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (266, 1, 11, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (267, 1, 11, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (268, 1, 11, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (269, 1, 11, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (270, 1, 11, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (271, 1, 12, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (272, 1, 12, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (273, 1, 12, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (274, 1, 12, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (275, 1, 12, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (276, 1, 12, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (277, 1, 12, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (278, 1, 12, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (279, 1, 12, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (280, 1, 12, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (281, 1, 12, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (282, 1, 12, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (283, 1, 12, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (284, 1, 12, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (285, 1, 12, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (286, 1, 12, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (287, 1, 12, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (288, 1, 12, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (289, 1, 12, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (290, 1, 12, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (291, 1, 13, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (292, 1, 13, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (293, 1, 13, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (294, 1, 13, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (295, 1, 13, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (296, 1, 13, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (297, 1, 13, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (298, 1, 13, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (299, 1, 13, 9, 0)
GO
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (300, 1, 13, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (301, 1, 13, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (302, 1, 13, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (303, 1, 13, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (304, 1, 13, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (305, 1, 13, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (306, 1, 13, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (307, 1, 13, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (308, 1, 13, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (309, 1, 13, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (310, 1, 13, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (311, 1, 14, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (312, 1, 14, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (313, 1, 14, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (314, 1, 14, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (315, 1, 14, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (316, 1, 14, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (317, 1, 14, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (318, 1, 14, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (319, 1, 14, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (320, 1, 14, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (321, 1, 14, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (322, 1, 14, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (323, 1, 14, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (324, 1, 14, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (325, 1, 14, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (326, 1, 14, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (327, 1, 14, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (328, 1, 14, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (329, 1, 14, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (330, 1, 14, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (331, 1, 15, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (332, 1, 15, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (333, 1, 15, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (334, 1, 15, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (335, 1, 15, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (336, 1, 15, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (337, 1, 15, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (338, 1, 15, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (339, 1, 15, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (340, 1, 15, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (341, 1, 15, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (342, 1, 15, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (343, 1, 15, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (344, 1, 15, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (345, 1, 15, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (346, 1, 15, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (347, 1, 15, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (348, 1, 15, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (349, 1, 15, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (350, 1, 15, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (351, 1, 16, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (352, 1, 16, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (353, 1, 16, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (354, 1, 16, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (355, 1, 16, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (356, 1, 16, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (357, 1, 16, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (358, 1, 16, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (359, 1, 16, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (360, 1, 16, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (361, 1, 16, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (362, 1, 16, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (363, 1, 16, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (364, 1, 16, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (365, 1, 16, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (366, 1, 16, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (367, 1, 16, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (368, 1, 16, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (369, 1, 16, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (370, 1, 16, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (371, 1, 17, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (372, 1, 17, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (373, 1, 17, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (374, 1, 17, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (375, 1, 17, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (376, 1, 17, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (377, 1, 17, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (378, 1, 17, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (379, 1, 17, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (380, 1, 17, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (381, 1, 17, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (382, 1, 17, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (383, 1, 17, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (384, 1, 17, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (385, 1, 17, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (386, 1, 17, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (387, 1, 17, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (388, 1, 17, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (389, 1, 17, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (390, 1, 17, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (391, 1, 18, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (392, 1, 18, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (393, 1, 18, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (394, 1, 18, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (395, 1, 18, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (396, 1, 18, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (397, 1, 18, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (398, 1, 18, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (399, 1, 18, 9, 0)
GO
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (400, 1, 18, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (401, 1, 18, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (402, 1, 18, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (403, 1, 19, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (404, 1, 19, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (405, 1, 19, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (406, 1, 19, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (407, 1, 19, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (408, 1, 19, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (409, 1, 19, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (410, 1, 19, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (411, 1, 19, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (412, 1, 19, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (413, 1, 19, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (414, 1, 19, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (415, 1, 20, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (416, 1, 20, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (417, 1, 20, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (418, 1, 20, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (419, 1, 20, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (420, 1, 20, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (421, 1, 20, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (422, 1, 20, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (423, 1, 20, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (424, 1, 20, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (425, 1, 20, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (426, 1, 20, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (427, 1, 21, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (428, 1, 21, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (429, 1, 21, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (430, 1, 21, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (431, 1, 21, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (432, 1, 21, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (433, 1, 21, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (434, 1, 21, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (435, 1, 21, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (436, 1, 21, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (437, 1, 21, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (438, 1, 21, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (439, 1, 22, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (440, 1, 22, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (441, 1, 22, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (442, 1, 22, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (443, 1, 22, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (444, 1, 22, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (445, 1, 22, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (446, 1, 22, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (447, 1, 22, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (448, 1, 22, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (449, 1, 22, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (450, 1, 22, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (451, 2, 23, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (452, 2, 23, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (453, 2, 23, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (454, 2, 23, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (455, 2, 23, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (456, 2, 23, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (457, 2, 23, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (458, 2, 23, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (459, 2, 23, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (460, 2, 23, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (461, 2, 23, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (462, 2, 23, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (463, 2, 23, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (464, 2, 23, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (465, 2, 24, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (466, 2, 24, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (467, 2, 24, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (468, 2, 24, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (469, 2, 24, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (470, 2, 24, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (471, 2, 24, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (472, 2, 24, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (473, 2, 24, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (474, 2, 24, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (475, 2, 24, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (476, 2, 24, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (477, 2, 24, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (478, 2, 24, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (479, 2, 25, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (480, 2, 25, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (481, 2, 25, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (482, 2, 25, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (483, 2, 25, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (484, 2, 25, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (485, 2, 25, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (486, 2, 25, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (487, 2, 25, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (488, 2, 25, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (489, 2, 25, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (490, 2, 25, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (491, 2, 25, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (492, 2, 25, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (493, 2, 26, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (494, 2, 26, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (495, 2, 26, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (496, 2, 26, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (497, 2, 26, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (498, 2, 26, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (499, 2, 26, 7, 0)
GO
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (500, 2, 26, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (501, 2, 26, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (502, 2, 26, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (503, 2, 26, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (504, 2, 26, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (505, 2, 26, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (506, 2, 26, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (507, 2, 27, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (508, 2, 27, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (509, 2, 27, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (510, 2, 27, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (511, 2, 27, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (512, 2, 27, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (513, 2, 27, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (514, 2, 27, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (515, 2, 27, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (516, 2, 27, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (517, 2, 27, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (518, 2, 27, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (519, 2, 27, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (520, 2, 27, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (521, 2, 28, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (522, 2, 28, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (523, 2, 28, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (524, 2, 28, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (525, 2, 28, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (526, 2, 28, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (527, 2, 28, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (528, 2, 28, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (529, 2, 28, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (530, 2, 28, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (531, 2, 28, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (532, 2, 28, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (533, 2, 28, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (534, 2, 28, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (535, 2, 29, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (536, 2, 29, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (537, 2, 29, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (538, 2, 29, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (539, 2, 29, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (540, 2, 29, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (541, 2, 29, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (542, 2, 29, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (543, 2, 29, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (544, 2, 30, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (545, 2, 30, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (546, 2, 30, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (547, 2, 30, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (548, 2, 30, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (549, 2, 30, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (550, 2, 30, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (551, 2, 30, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (552, 2, 30, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (553, 2, 31, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (554, 2, 31, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (555, 2, 31, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (556, 2, 31, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (557, 2, 31, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (558, 2, 31, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (559, 2, 31, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (560, 2, 31, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (561, 2, 31, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (562, 2, 32, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (563, 2, 32, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (564, 2, 32, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (565, 2, 32, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (566, 2, 32, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (567, 2, 32, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (568, 2, 32, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (569, 2, 32, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (570, 2, 32, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (571, 2, 33, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (572, 2, 33, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (573, 2, 33, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (574, 2, 33, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (575, 2, 33, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (576, 2, 33, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (577, 2, 33, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (578, 2, 33, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (579, 2, 33, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (580, 2, 34, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (581, 2, 34, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (582, 2, 34, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (583, 2, 34, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (584, 2, 34, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (585, 2, 34, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (586, 2, 34, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (587, 2, 35, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (588, 2, 35, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (589, 2, 35, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (590, 2, 35, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (591, 2, 35, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (592, 2, 35, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (593, 2, 35, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (594, 2, 36, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (595, 2, 36, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (596, 2, 36, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (597, 2, 36, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (598, 2, 36, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (599, 2, 36, 6, 0)
GO
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (600, 2, 36, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (601, 2, 37, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (602, 2, 37, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (603, 2, 37, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (604, 2, 37, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (605, 2, 37, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (606, 2, 37, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (607, 2, 37, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (608, 3, 38, 1, 1)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (609, 3, 38, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (610, 3, 38, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (611, 3, 38, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (612, 3, 38, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (613, 3, 38, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (614, 3, 38, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (615, 3, 38, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (616, 3, 38, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (617, 3, 38, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (618, 3, 38, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (619, 3, 38, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (620, 3, 38, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (621, 3, 38, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (622, 3, 38, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (623, 3, 38, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (624, 3, 38, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (625, 3, 38, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (626, 3, 38, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (627, 3, 38, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (628, 3, 39, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (629, 3, 39, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (630, 3, 39, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (631, 3, 39, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (632, 3, 39, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (633, 3, 39, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (634, 3, 39, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (635, 3, 39, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (636, 3, 39, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (637, 3, 39, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (638, 3, 39, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (639, 3, 39, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (640, 3, 39, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (641, 3, 39, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (642, 3, 39, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (643, 3, 39, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (644, 3, 39, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (645, 3, 39, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (646, 3, 39, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (647, 3, 39, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (648, 3, 40, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (649, 3, 40, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (650, 3, 40, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (651, 3, 40, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (652, 3, 40, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (653, 3, 40, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (654, 3, 40, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (655, 3, 40, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (656, 3, 40, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (657, 3, 40, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (658, 3, 40, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (659, 3, 40, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (660, 3, 40, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (661, 3, 40, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (662, 3, 40, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (663, 3, 40, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (664, 3, 40, 17, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (665, 3, 40, 18, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (666, 3, 40, 19, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (667, 3, 40, 20, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (668, 3, 41, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (669, 3, 41, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (670, 3, 41, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (671, 3, 41, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (672, 3, 41, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (673, 3, 41, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (674, 3, 41, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (675, 3, 41, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (676, 3, 41, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (677, 3, 41, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (678, 3, 41, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (679, 3, 41, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (680, 3, 41, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (681, 3, 41, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (682, 3, 41, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (683, 3, 41, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (684, 3, 42, 1, 1)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (685, 3, 42, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (686, 3, 42, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (687, 3, 42, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (688, 3, 42, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (689, 3, 42, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (690, 3, 42, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (691, 3, 42, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (692, 3, 42, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (693, 3, 42, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (694, 3, 42, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (695, 3, 42, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (696, 3, 42, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (697, 3, 42, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (698, 3, 42, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (699, 3, 42, 16, 0)
GO
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (700, 3, 43, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (701, 3, 43, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (702, 3, 43, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (703, 3, 43, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (704, 3, 43, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (705, 3, 43, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (706, 3, 43, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (707, 3, 43, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (708, 3, 43, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (709, 3, 43, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (710, 3, 43, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (711, 3, 43, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (712, 3, 43, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (713, 3, 43, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (714, 3, 43, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (715, 3, 43, 16, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (716, 3, 44, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (717, 3, 44, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (718, 3, 44, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (719, 3, 44, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (720, 3, 44, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (721, 3, 44, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (722, 3, 44, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (723, 3, 44, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (724, 3, 44, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (725, 3, 44, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (726, 3, 44, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (727, 3, 44, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (728, 3, 44, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (729, 3, 44, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (730, 3, 44, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (731, 3, 45, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (732, 3, 45, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (733, 3, 45, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (734, 3, 45, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (735, 3, 45, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (736, 3, 45, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (737, 3, 45, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (738, 3, 45, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (739, 3, 45, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (740, 3, 45, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (741, 3, 45, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (742, 3, 45, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (743, 3, 45, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (744, 3, 45, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (745, 3, 45, 15, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (746, 3, 46, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (747, 3, 46, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (748, 3, 46, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (749, 3, 46, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (750, 3, 46, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (751, 3, 46, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (752, 3, 46, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (753, 3, 46, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (754, 3, 46, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (755, 3, 46, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (756, 3, 46, 11, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (757, 3, 46, 12, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (758, 3, 46, 13, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (759, 3, 46, 14, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (760, 3, 46, 15, 0)
SET IDENTITY_INSERT [dbo].[kursi] OFF
INSERT [dbo].[pegawai] ([id_pegawai], [nama_pegawai], [tempat_lahir], [tanggal_lahir], [alamat], [no_tlp], [email], [stasiun_bekerja]) VALUES (N'PG-01', N'Admin', N'Ponorogo', CAST(N'1998-06-30' AS Date), N'Ponorogo', N'083845338679', N'Admin@gmail.com', N'Gambir')
INSERT [dbo].[pegawai] ([id_pegawai], [nama_pegawai], [tempat_lahir], [tanggal_lahir], [alamat], [no_tlp], [email], [stasiun_bekerja]) VALUES (N'PG-02', N'Dewi', N'Ponorogo', CAST(N'2016-02-01' AS Date), N'Ponorogo', N'082345678122', N'dewi@gmail.com', N'Madiun')
INSERT [dbo].[pegawai] ([id_pegawai], [nama_pegawai], [tempat_lahir], [tanggal_lahir], [alamat], [no_tlp], [email], [stasiun_bekerja]) VALUES (N'PG-03', N'Pak Gunaryoko', N'Ponorogo', CAST(N'1890-02-03' AS Date), N'Ponorogo', N'07756452323', N'djhdsjdj@gmail.com', N'Gambir')
INSERT [dbo].[penumpang] ([id_penumpang], [nama_penumpang], [no_identitas], [alamat], [jenis_kelamin], [email], [no_tlp]) VALUES (N'P-01', N'Dewi', N'923546374', N'Ponorogo', N'Perempuan', N'dewi@gmail.com', N'09185324564')
INSERT [dbo].[penumpang] ([id_penumpang], [nama_penumpang], [no_identitas], [alamat], [jenis_kelamin], [email], [no_tlp]) VALUES (N'P-02', N'Kumala', N'243435', N'Madiun', N'Perempuan', N'kumala@gmail.com', N'05636376373')
INSERT [dbo].[penumpang] ([id_penumpang], [nama_penumpang], [no_identitas], [alamat], [jenis_kelamin], [email], [no_tlp]) VALUES (N'P-03', N'Arif', N'925753724', N'ponorogo', N'Laki-Laki', N'sfsdha@gmail.com', N'09886357436')
INSERT [dbo].[stasiun] ([id_stasiun], [nama_stasiun], [kota], [provinsi]) VALUES (N'ST-01', N'Gambir', N'Jakarta', N'DKI Jakarta')
INSERT [dbo].[stasiun] ([id_stasiun], [nama_stasiun], [kota], [provinsi]) VALUES (N'ST-02', N'Madiun', N'Madiun', N'Jawa Timur')
INSERT [dbo].[stasiun] ([id_stasiun], [nama_stasiun], [kota], [provinsi]) VALUES (N'ST-03', N'Surabaya Gubeng', N'Surabaya', N'Jawa Timur')
INSERT [dbo].[stasiun] ([id_stasiun], [nama_stasiun], [kota], [provinsi]) VALUES (N'ST-04', N'Surabaya Kota', N'Surabaya', N'Jawa Timur')
INSERT [dbo].[stasiun] ([id_stasiun], [nama_stasiun], [kota], [provinsi]) VALUES (N'ST-05', N'Malang', N'Malang', N'Jawa Timur')
INSERT [dbo].[stasiun] ([id_stasiun], [nama_stasiun], [kota], [provinsi]) VALUES (N'ST-06', N'Wates', N'Wates', N'Yogyakarta')
INSERT [dbo].[transaksi] ([id_transaksi], [id_penumpang], [id_user], [id_kereta], [stasiun_awal], [stasiun_berhenti], [waktu_berangkat], [waktu_datang], [kelas], [no_gerbong], [no_kursi], [harga], [tgl_beli]) VALUES (N'TR-01', N'P-01', N'U01', 1, N'Gambir', N'Wates', N'9/11/2016 11:00:00 AM', N'9/11/2016 11:00:00 PM', N'Ekonomi', 1, 1, 100000, CAST(N'2016-09-12' AS Date))
INSERT [dbo].[transaksi] ([id_transaksi], [id_penumpang], [id_user], [id_kereta], [stasiun_awal], [stasiun_berhenti], [waktu_berangkat], [waktu_datang], [kelas], [no_gerbong], [no_kursi], [harga], [tgl_beli]) VALUES (N'TR-02', N'P-03', N'U01', 3, N'Gambir', N'Madiun', N'9/12/2016 4:00:31 AM', N'9/12/2016 4:00:31 AM', N'Ekonomi', 1, 1, 100000, CAST(N'2016-09-12' AS Date))
INSERT [dbo].[transaksi] ([id_transaksi], [id_penumpang], [id_user], [id_kereta], [stasiun_awal], [stasiun_berhenti], [waktu_berangkat], [waktu_datang], [kelas], [no_gerbong], [no_kursi], [harga], [tgl_beli]) VALUES (N'TR-03', N'P-03', N'U01', 3, N'Gambir', N'Surabaya Gubeng', N'9/12/2016 4:00:31 AM', N'9/12/2016 4:00:31 AM', N'Bisnis', 5, 13, 200000, CAST(N'2016-09-12' AS Date))
INSERT [dbo].[user] ([id_user], [id_pegawai], [id_akses], [username], [password]) VALUES (N'U01', N'PG-01', 1, N'Admin', N'admin123')
INSERT [dbo].[user] ([id_user], [id_pegawai], [id_akses], [username], [password]) VALUES (N'U02', N'PG-02', 2, N'dewi', N'arief')
ALTER TABLE [dbo].[gerbong]  WITH CHECK ADD  CONSTRAINT [FK_gerbong_kereta] FOREIGN KEY([id_kereta])
REFERENCES [dbo].[kereta] ([id_kereta])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[gerbong] CHECK CONSTRAINT [FK_gerbong_kereta]
GO
ALTER TABLE [dbo].[jadwal]  WITH CHECK ADD  CONSTRAINT [FK_jadwal_kereta] FOREIGN KEY([id_kereta])
REFERENCES [dbo].[kereta] ([id_kereta])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[jadwal] CHECK CONSTRAINT [FK_jadwal_kereta]
GO
ALTER TABLE [dbo].[kursi]  WITH CHECK ADD  CONSTRAINT [FK_kursi_gerbong] FOREIGN KEY([id_gerbong])
REFERENCES [dbo].[gerbong] ([id_gerbong])
GO
ALTER TABLE [dbo].[kursi] CHECK CONSTRAINT [FK_kursi_gerbong]
GO
ALTER TABLE [dbo].[kursi]  WITH CHECK ADD  CONSTRAINT [FK_kursi_kereta] FOREIGN KEY([id_kereta])
REFERENCES [dbo].[kereta] ([id_kereta])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[kursi] CHECK CONSTRAINT [FK_kursi_kereta]
GO
ALTER TABLE [dbo].[transaksi]  WITH CHECK ADD  CONSTRAINT [FK_transaksi_kereta] FOREIGN KEY([id_kereta])
REFERENCES [dbo].[kereta] ([id_kereta])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[transaksi] CHECK CONSTRAINT [FK_transaksi_kereta]
GO
ALTER TABLE [dbo].[transaksi]  WITH CHECK ADD  CONSTRAINT [FK_transaksi_penumpang] FOREIGN KEY([id_penumpang])
REFERENCES [dbo].[penumpang] ([id_penumpang])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[transaksi] CHECK CONSTRAINT [FK_transaksi_penumpang]
GO
ALTER TABLE [dbo].[transaksi]  WITH CHECK ADD  CONSTRAINT [FK_transaksi_user] FOREIGN KEY([id_user])
REFERENCES [dbo].[user] ([id_user])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[transaksi] CHECK CONSTRAINT [FK_transaksi_user]
GO
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [FK_user_akses] FOREIGN KEY([id_akses])
REFERENCES [dbo].[akses] ([id_akses])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [FK_user_akses]
GO
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [FK_user_pegawai] FOREIGN KEY([id_pegawai])
REFERENCES [dbo].[pegawai] ([id_pegawai])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [FK_user_pegawai]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "akses"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 120
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "user"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pegawai"
            Begin Extent = 
               Top = 6
               Left = 454
               Bottom = 155
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_akun'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_akun'
GO
USE [master]
GO
ALTER DATABASE [db_kereta] SET  READ_WRITE 
GO
