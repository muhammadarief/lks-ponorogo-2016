﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FormLogin

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call bukaDB()
        TextBox2.UseSystemPasswordChar = True
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Label2.Text = TimeOfDay
        Label1.Text = DateString
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            TextBox2.UseSystemPasswordChar = False
        Else
            TextBox2.UseSystemPasswordChar = True
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        CMD = New SqlCommand("SELECT * FROM [user] where username='" & TextBox1.Text & "' and password='" & TextBox2.Text & "'", konek)
        DR = CMD.ExecuteReader
        If TextBox1.Text = "" Then
            MsgBox("Username harus diisi!!", vbCritical)
            TextBox1.Text = ""
            TextBox1.Focus()
        ElseIf TextBox2.Text = "" Then
            MsgBox("Password harus diisi!!", vbCritical)
            TextBox2.Text = ""
            TextBox2.Focus()
        End If
        If DR.Read Then
            MsgBox("Login Berhasil!!", vbInformation)
            Form2.Show()
            Me.Hide()
        Else
            MsgBox("Username dan Password salah!!", vbInformation)
            TextBox1.Text = ""
            TextBox2.Text = ""
            TextBox1.Focus()
        End If
        Form2.Show()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan Keluar?", vbYesNo + vbCritical)
        If pesan = vbYes Then
            Me.Close()
        End If
    End Sub
End Class
