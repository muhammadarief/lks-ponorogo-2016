﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FormGantiPassword

    Private Sub FormGantiPassword_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bukaDB()
        TextBox1.UseSystemPasswordChar = True
        TextBox2.UseSystemPasswordChar = True
        TextBox3.UseSystemPasswordChar = True
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            TextBox1.UseSystemPasswordChar = False
        Else
            TextBox1.UseSystemPasswordChar = True
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked Then
            TextBox2.UseSystemPasswordChar = False
        Else
            TextBox2.UseSystemPasswordChar = True
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked Then
            TextBox3.UseSystemPasswordChar = False
        Else
            TextBox3.UseSystemPasswordChar = True
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan Keluar dari form ganti Password?", vbYesNo + vbCritical)
        If pesan = vbYes Then
            Me.Dispose()
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        CMD = New SqlCommand("SELECT COUNT(*) from [user] where password='" & TextBox1.Text & "' and username='" & FormLogin.TextBox1.Text & "'", konek)
        Dim DR As Integer = CMD.ExecuteScalar

        If (TextBox1.Text = "" And TextBox2.Text = "" And TextBox3.Text = "") Then
            MsgBox("Password Belum diisi", vbCritical)
        ElseIf (TextBox2.Text <> TextBox3.Text) Then
            MsgBox("Konfirmasi Password tidak sesuai!!", vbCritical)
        ElseIf (DR = 0) Then
            MsgBox("Password Salah!!", vbCritical)
        Else
            CMD = New SqlCommand("update [user] set password='" & TextBox3.Text & "' where username='" & FormLogin.TextBox1.Text & "'", konek)
            DR = CMD.ExecuteNonQuery
            MsgBox("Password Berhasil di ganti!!", vbInformation)
        End If

    End Sub
End Class