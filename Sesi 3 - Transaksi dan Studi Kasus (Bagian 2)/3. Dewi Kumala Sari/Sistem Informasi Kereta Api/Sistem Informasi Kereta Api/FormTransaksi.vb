﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FormTransaksi

    Private Sub GantiPasswordToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GantiPasswordToolStripMenuItem.Click
        FormGantiPassword.Show()
    End Sub

    Private Sub TambahKeretaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahKeretaToolStripMenuItem.Click
        FormTambahKereta.Show()
        Me.Dispose()
    End Sub

    Private Sub TambahStasiunToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahStasiunToolStripMenuItem.Click
        FormTambahStasiun.Show()
        Me.Dispose()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        FormTambahUser.Show()
        Me.Dispose()

    End Sub

    Private Sub TambahJadwalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahJadwalToolStripMenuItem.Click
        FormTambahJadwal.Show()
        Me.Dispose()
    End Sub

    Private Sub DataPenumpangToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DataPenumpangToolStripMenuItem.Click
        FormTambahPenumpang.Show()
        Me.Close()
    End Sub

    Private Sub TransaksiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TransaksiToolStripMenuItem.Click
        Me.Show()

    End Sub

    Dim databaru As Boolean

    Sub idtransaksi()
        bukaDB()
        TextBox1.Enabled = False
        Dim nilai1 As String = ""
        Dim nilai2 As String = ""
        CMD = New SqlCommand("select * from [transaksi] order by id_transaksi desc", konek)
        DR = CMD.ExecuteReader
        If DR.Read() Then
            nilai1 = Mid(DR.Item("id_transaksi"), 4, 2)
            nilai2 = Val(nilai1) + 1
            TextBox1.Text = "TR-" + Mid("0", 1, 2 - nilai2.Length) & nilai2
        Else
            TextBox1.Text = "TR-01"
        End If
    End Sub

    Private Sub FormTransaksi_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bukaDB()
        idtransaksi()
        isigrid()
        TextBox2.Enabled = False
        TextBox3.Enabled = False
        DateTimePicker1.Enabled = False

        CMD = New SqlCommand("SELECT * from [penumpang]", konek)
        DR = CMD.ExecuteReader
        While DR.Read
            ComboBox1.Items.Add(DR.Item("nama_penumpang"))
        End While

        Dim CMD2 = New SqlCommand("SELECT * from [user] where username='" & FormLogin.TextBox1.Text & "'", konek)
        Dim DR2 = CMD2.ExecuteReader
        If DR2.HasRows Then
            If DR2.Read Then
                TextBox2.Text = DR2.Item("username")
            End If
        End If

        Dim CMD1 = New SqlCommand("SELECT * from [kereta]", konek)
        Dim DR1 = CMD1.ExecuteReader
        While DR1.Read
            ComboBox2.Items.Add(DR1.Item("nama_kereta"))
        End While

        Dim CMD3 = New SqlCommand("SELECT * from [jadwal]", konek)
        Dim DR3 = CMD3.ExecuteReader
        While DR3.Read
            ComboBox3.Items.Add(DR3.Item("stasiun_awal"))
        End While

        Dim CMD4 = New SqlCommand("SELECT DISTINCT(kelas) from [gerbong]", konek)
        Dim DR4 = CMD4.ExecuteReader
        While DR4.Read
            ComboBox7.Items.Add(DR4.Item("kelas"))
        End While
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox3.SelectedIndexChanged
        Dim CMD3 = New SqlCommand("SELECT * from [jadwal] where stasiun_awal='" & ComboBox3.SelectedItem & "'", konek)
        Dim DR3 = CMD3.ExecuteReader
        While DR3.Read
            ComboBox4.Items.Add(DR3.Item("stasiun_berhenti"))
            ComboBox5.Items.Add(DR3.Item("waktu_berangkat"))
            ComboBox6.Items.Add(DR3.Item("waktu_datang"))
        End While
    End Sub

    Private Sub ComboBox7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox7.SelectedIndexChanged
        CMD = New SqlCommand("SELECT id_kereta from [kereta] where nama_kereta = '" & ComboBox2.SelectedItem & "'", konek)
        Dim DR As Integer = CMD.ExecuteScalar

        Dim CMD2 = New SqlCommand("SELECT * from [gerbong] where id_kereta='" & DR & "' and kelas='" & ComboBox7.SelectedItem & "'", konek)
        Dim DR2 = CMD2.ExecuteReader
        If DR2.HasRows Then
            If DR2.Read Then
                TextBox3.Text = DR2.Item("harga")
            End If
        End If

        Dim CMD1 = New SqlCommand("SELECT * from [gerbong] where id_kereta='" & DR & "' and kelas='" & ComboBox7.SelectedItem & "'", konek)
        Dim DR1 = CMD1.ExecuteReader
        ComboBox8.Items.Clear()
        While DR1.Read
            ComboBox8.Items.Add(DR1.Item("no_gerbong"))
        End While

    End Sub

    Private Sub ComboBox8_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox8.SelectedIndexChanged
        CMD = New SqlCommand("SELECT id_kereta from [kereta] where nama_kereta = '" & ComboBox2.SelectedItem & "'", konek)
        Dim DR As Integer = CMD.ExecuteScalar

        Dim CMD2 = New SqlCommand("SELECT id_gerbong from [gerbong] where id_kereta='" & DR & "' and kelas='" & ComboBox7.SelectedItem & "' and no_gerbong='" & ComboBox8.SelectedItem & "'", konek)
        Dim DR2 As Integer = CMD2.ExecuteScalar

        Dim CMD1 = New SqlCommand("SELECT * from [kursi] where id_kereta='" & DR & "' and id_gerbong='" & DR2 & "' and status_kursi=0", konek)
        Dim DR1 = CMD1.ExecuteReader
        ComboBox9.Items.Clear()
        While DR1.Read
            ComboBox9.Items.Add(DR1.Item("no_kursi"))
        End While
    End Sub

    Sub isigrid()
        bukaDB()
        DA = New SqlDataAdapter("select * from [transaksi]", konek)
        DS = New DataSet
        DS.Clear()
        DA.Fill(DS, "transaksi")
        DataGridView1.DataSource = (DS.Tables("transaksi"))
        DataGridView1.Enabled = True
    End Sub

    Private Sub jalankansql(ByVal sQL As String)
        Dim objcmd As New System.Data.SqlClient.SqlCommand
        Try
            objcmd.Connection = konek
            objcmd.CommandType = CommandType.Text
            objcmd.CommandText = sQL
            objcmd.ExecuteNonQuery()
            objcmd.Dispose()
            MsgBox("Data Berhasil di simpan!!", vbInformation)
        Catch ex As Exception
            MsgBox("Data Gagal disimpan!!" & ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        databaru = True

        CMD = New SqlCommand("SELECT id_kereta from [kereta] where nama_kereta = '" & ComboBox2.SelectedItem & "'", konek)
        Dim DR As Integer = CMD.ExecuteScalar

        Dim CMD2 = New SqlCommand("SELECT id_gerbong from [gerbong] where id_kereta='" & DR & "' and kelas='" & ComboBox7.SelectedItem & "' and no_gerbong='" & ComboBox8.SelectedItem & "'", konek)
        Dim DR2 As Integer = CMD2.ExecuteScalar

        Dim CMD1 = New SqlCommand("SELECT id_kursi from [kursi] where id_kereta='" & DR & "' and id_gerbong='" & DR2 & "'", konek)
        Dim DR1 As Integer = CMD1.ExecuteScalar

        Dim CMD3 = New SqlCommand("SELECT id_user from [user] where username='" & TextBox2.Text & "'", konek)
        Dim DR3 = CMD3.ExecuteScalar

        Dim CMD4 = New SqlCommand("SELECT id_penumpang from [penumpang] where nama_penumpang='" & ComboBox1.SelectedItem & "'", konek)
        Dim DR4 = CMD4.ExecuteScalar

        Dim simpan, simpan1 As String
        Dim pesan As String
        Dim date1, tgl1, bln1, thn1 As String

        thn1 = DateTimePicker1.Value.Year
        bln1 = DateTimePicker1.Value.Month
        tgl1 = DateTimePicker1.Value.Day

        date1 = thn1 & "-" & bln1 & "-" & tgl1

        If TextBox2.Text = "" And TextBox3.Text = "" Then Exit Sub

        If databaru Then
            pesan = MsgBox("Apakah anda yakin akan menambah data ke database??", vbYesNo + vbQuestion)
            If pesan = vbYesNo Then
                Exit Sub
            End If
            simpan = "insert into [transaksi](id_transaksi,id_penumpang,id_user,id_kereta,stasiun_awal,stasiun_berhenti,waktu_berangkat,waktu_datang,kelas,no_gerbong,no_kursi,harga,tgl_beli) values ('" & TextBox1.Text & "','" & DR4 & "','" & DR3 & "','" & DR & "','" & ComboBox3.SelectedItem & "','" & ComboBox4.SelectedItem & "','" & ComboBox5.SelectedItem & "','" & ComboBox6.SelectedItem & "','" & ComboBox7.SelectedItem & "','" & ComboBox8.SelectedItem & "','" & ComboBox9.SelectedItem & "','" & TextBox3.Text & "','" & date1 & "')"
            simpan1 = " update [kursi] set status_kursi=1 where id_kursi='" & DR1 & "'"

        Else
            pesan = MsgBox("Apakah anda yakin akan mengupdate database??", vbYesNo + vbQuestion)
            If pesan = vbYesNo Then
                Exit Sub
            End If
            simpan = "update [transaksi] set " _
                + "id_penumpang ='" & DR4 & "'," _
                + "id_user ='" & DR3 & "'," _
                + "id_kereta ='" & DR & "'," _
                + "stasiun_awal ='" & ComboBox3.SelectedItem & "'," _
                + "stasiun_berhenti ='" & ComboBox4.SelectedItem & "'," _
                + "waktu_berangkat ='" & ComboBox5.SelectedItem & "'," _
                + "waktu_datang ='" & ComboBox6.SelectedItem & "'," _
                + "kelas ='" & ComboBox7.SelectedItem & "'," _
                + "no_gerbong='" & ComboBox8.SelectedItem & "'," _
                + "no_kursi ='" & ComboBox9.SelectedItem & "'," _
                + "harga ='" & TextBox3.Text & "'," _
                + "tgl_beli='" & date1 & "' where id_penumpang='" & TextBox1.Text & "'"
            simpan1 = " update [kursi] set status_kursi=1 where id_kursi='" & DR1 & "'"
        End If
        jalankansql(simpan)
        jalankansql(simpan1)
        DataGridView1.Refresh()
        isigrid()

    End Sub

    Private Sub BulananToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BulananToolStripMenuItem.Click
        FormLaporan.Show()
    End Sub

    Private Sub LogOutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LogOutToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan Log Out?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            FormLogin.Show()
            Me.Dispose()
        End If
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KeluarToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan keluar?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            Me.Close()
        End If
    End Sub
End Class