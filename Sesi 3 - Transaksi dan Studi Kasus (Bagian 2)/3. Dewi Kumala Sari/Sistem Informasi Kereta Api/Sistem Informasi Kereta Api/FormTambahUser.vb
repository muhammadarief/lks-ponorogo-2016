﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FormTambahUser
    Dim databaru As Boolean

    Sub idpegawai()
        bukaDB()
        TextBox1.Enabled = False
        Dim nilai1 As String = ""
        Dim nilai2 As String = ""
        CMD = New SqlCommand("select * from [pegawai] order by id_pegawai desc", konek)
        DR = CMD.ExecuteReader
        If DR.Read() Then
            nilai1 = Mid(DR.Item("id_pegawai"), 4, 2)
            nilai2 = Val(nilai1) + 1
            TextBox1.Text = "PG-" + Mid("0", 1, 2 - nilai2.Length) & nilai2
        Else
            TextBox1.Text = "PG-01"
        End If
        TextBox2.Focus()
    End Sub

    Sub iduser()
        bukaDB()
        TextBox7.Enabled = False
        Dim nilai1 As String = ""
        Dim nilai2 As String = ""
        CMD = New SqlCommand("select * from [user] order by id_user desc", konek)
        DR = CMD.ExecuteReader
        If DR.Read() Then
            nilai1 = Mid(DR.Item("id_user"), 2, 2)
            nilai2 = Val(nilai1) + 1
            TextBox7.Text = "U" + Mid("0", 1, 2 - nilai2.Length) & nilai2
        Else
            TextBox7.Text = "U01"
        End If
    End Sub

    Private Sub FormTambahUser_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bukaDB()
        isigrid()
        idpegawai()
        iduser()
        TextBox9.UseSystemPasswordChar = True

        CMD = New SqlCommand("SELECT * from [akses]", konek)
        DR = CMD.ExecuteReader
        While DR.Read
            ComboBox2.Items.Add(DR.Item("id_akses"))
        End While

        Dim CMD1 = New SqlCommand("SELECT * from [stasiun]", konek)
        Dim DR1 = CMD1.ExecuteReader
        While DR1.Read
            ComboBox1.Items.Add(DR1.Item("nama_stasiun"))
        End While
    End Sub
    Sub isigrid()
        bukaDB()
        DA = New SqlDataAdapter("select * from [v_akun]", konek)
        DS = New DataSet
        DS.Clear()
        DA.Fill(DS, "v_akun")
        DataGridView1.DataSource = (DS.Tables("v_akun"))
        DataGridView1.Enabled = True
    End Sub

    Private Sub jalankansql(ByVal sQL As String)
        Dim objcmd As New System.Data.SqlClient.SqlCommand
        Try
            objcmd.Connection = konek
            objcmd.CommandType = CommandType.Text
            objcmd.CommandText = sQL
            objcmd.ExecuteNonQuery()
            objcmd.Dispose()
            MsgBox("Data Berhasil di simpan!!", vbInformation)
        Catch ex As Exception
            MsgBox("Data Gagal disimpan!!" & ex.Message)
        End Try
    End Sub

    Private Sub GantiPasswordToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GantiPasswordToolStripMenuItem.Click
        FormGantiPassword.Show()
    End Sub

    Private Sub TambahKeretaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahKeretaToolStripMenuItem.Click
        FormTambahKereta.Show()
        Me.Dispose()
    End Sub

    Private Sub TambahStasiunToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahStasiunToolStripMenuItem.Click
        FormTambahStasiun.Show()
        Me.Dispose()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click

        Me.Show()

    End Sub
    Private Sub TambahJadwalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahJadwalToolStripMenuItem.Click
        FormTambahJadwal.Show()
        Me.Dispose()
    End Sub

    Private Sub DataPenumpangToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DataPenumpangToolStripMenuItem.Click
        FormTambahPenumpang.Show()
        Me.Close()
    End Sub

    Private Sub TransaksiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TransaksiToolStripMenuItem.Click
        FormTransaksi.Show()
        Me.Dispose()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        databaru = True
        Dim simpan1, simpan2 As String
        Dim pesan As String
        Dim date1, tgl1, bln1, thn1 As String

        thn1 = DateTimePicker1.Value.Year
        bln1 = DateTimePicker1.Value.Month
        tgl1 = DateTimePicker1.Value.Day

        date1 = thn1 & "-" & bln1 & "-" & tgl1

        If TextBox2.Text = "" And TextBox2.Text = "" Then Exit Sub

        If databaru Then

            pesan = MsgBox("Apakah anda yakin akan menambah data ke database??", vbYesNo + vbQuestion)
            If pesan = vbYesNo Then
                Exit Sub
            End If

            simpan1 = "insert into [pegawai](id_pegawai,nama_pegawai,tempat_lahir,tanggal_lahir,alamat,no_tlp,email,stasiun_bekerja) values ('" & TextBox1.Text & "','" & TextBox2.Text & "','" & TextBox3.Text & "','" & date1 & "','" & TextBox4.Text & "','" & TextBox5.Text & "','" & TextBox6.Text & "','" & ComboBox1.SelectedItem & "')"
            simpan2 = "insert into [user](id_user,id_pegawai,id_akses,username,password) values ('" & TextBox7.Text & "','" & TextBox1.Text & "','" & ComboBox2.SelectedItem & "','" & TextBox8.Text & "','" & TextBox9.Text & "')"
            bersih()
        Else
            pesan = MsgBox("Apakah anda yakin akan mengupdate data ke database??", vbYesNo + vbQuestion)
            If pesan = vbYesNo Then
                Exit Sub
            End If

            simpan1 = "update [pegawai] set " _
                + "nama_pegawai ='" & TextBox2.Text & "'," _
                + "tempat_lahir ='" & TextBox3.Text & "'," _
                + "tanggal_lahir ='" & date1 & "'," _
                + "alamat ='" & TextBox4.Text & "'," _
                + "no_tlp ='" & TextBox5.Text & "'," _
                + "email ='" & TextBox6.Text & "'," _
                + "stasiun_bekerja ='" & ComboBox1.SelectedItem & "' where id_pegawai='" & TextBox1.Text & "'"

            simpan2 = "update [user] set " _
                + "id_pegawai ='" & TextBox1.Text & "'," _
                + "id_akses ='" & ComboBox2.SelectedItem & "'," _
                + "username ='" & TextBox8.Text & "'," _
                + "password='" & TextBox9.Text & "' where id_user='" & TextBox7.Text & "'"
            bersih()

        End If
        jalankansql(simpan1)
        jalankansql(simpan2)
        DataGridView1.Refresh()
        isigrid()
    End Sub

    Private Sub TextBox5_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox5.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then MsgBox("Input Harus Angka")
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            TextBox9.UseSystemPasswordChar = False
        Else
            TextBox9.UseSystemPasswordChar = True
        End If
    End Sub

    Private Sub BulananToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BulananToolStripMenuItem.Click
        FormLaporan.Show()
    End Sub

    Private Sub LogOutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LogOutToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan Log Out?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            FormLogin.Show()
            Me.Dispose()
        End If
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KeluarToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan keluar?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            Me.Close()
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim hapussql As String
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan nenghapus data dari server... '" & TextBox2.Text & "' ", vbYesNo + vbExclamation, "PERHATIAN!!")
        hapussql = "delete from [user] where id_user='" & TextBox7.Text & "'"

        jalankansql(hapussql)
        DataGridView1.Refresh()
        isigrid()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim simpan1, simpan2 As String
        Dim pesan As String
        Dim date1, tgl1, bln1, thn1 As String

        thn1 = DateTimePicker1.Value.Year
        bln1 = DateTimePicker1.Value.Month
        tgl1 = DateTimePicker1.Value.Day

        date1 = thn1 & "-" & bln1 & "-" & tgl1

        pesan = MsgBox("Apakah anda yakin akan mengupdate data ke database??", vbYesNo + vbQuestion)
        If pesan = vbYesNo Then
            Exit Sub
        End If

        simpan1 = "update [pegawai] set " _
            + "nama_pegawai ='" & TextBox2.Text & "'," _
            + "tempat_lahir ='" & TextBox3.Text & "'," _
            + "tanggal_lahir ='" & date1 & "'," _
            + "alamat ='" & TextBox4.Text & "'," _
            + "no_tlp ='" & TextBox5.Text & "'," _
            + "email ='" & TextBox6.Text & "'," _
            + "stasiun_bekerja ='" & ComboBox1.SelectedItem & "' where id_pegawai='" & TextBox1.Text & "'"

        simpan2 = "update [user] set " _
            + "id_pegawai ='" & TextBox1.Text & "'," _
            + "id_akses ='" & ComboBox2.SelectedItem & "'," _
            + "username ='" & TextBox8.Text & "'," _
            + "password='" & TextBox9.Text & "' where id_user='" & TextBox7.Text & "'"

        bersih()

        jalankansql(simpan1)
        jalankansql(simpan2)
        DataGridView1.Refresh()
        isigrid()
    End Sub

    Sub bersih()
        idpegawai()
        iduser()
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox8.Text = ""
        TextBox9.Text = ""
        ComboBox1.Text = ""
        ComboBox2.Text = ""

    End Sub
End Class