﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FormTambahPenumpang
    Dim databaru As Boolean

    Sub idpenumpang()
        bukaDB()
        TextBox1.Enabled = False
        Dim nilai1 As String = ""
        Dim nilai2 As String = ""
        CMD = New SqlCommand("select * from [penumpang] order by id_penumpang desc", konek)
        DR = CMD.ExecuteReader
        If DR.Read() Then
            nilai1 = Mid(DR.Item("id_penumpang"), 3, 2)
            nilai2 = Val(nilai1) + 1
            TextBox1.Text = "P-" + Mid("0", 1, 2 - nilai2.Length) & nilai2
        Else
            TextBox1.Text = "P-01"
        End If
        TextBox2.Focus()
    End Sub

    Private Sub TextBox6_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox6.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then MsgBox("Input Harus Angka")
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Sub TextBox3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox3.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then MsgBox("Input Harus Angka")
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack) Then e.Handled = True
    End Sub

    Private Function jeniskelamin() As String
        For Each RadiButton As Control In GroupBox2.Controls
            If CType(RadiButton, RadioButton).Checked Then
                Return CType(RadiButton, RadioButton).Text
            End If
        Next
        Return ""
    End Function

    Private Sub isitextbox(ByVal x As Integer)
        Try
            TextBox1.Text = DataGridView1.Rows(x).Cells(0).Value
            TextBox2.Text = DataGridView1.Rows(x).Cells(1).Value
            TextBox3.Text = DataGridView1.Rows(x).Cells(2).Value
            TextBox4.Text = DataGridView1.Rows(x).Cells(3).Value
            TextBox5.Text = DataGridView1.Rows(x).Cells(4).Value
            TextBox6.Text = DataGridView1.Rows(x).Cells(5).Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub FormTambahPenumpang_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bukaDB()
        idpenumpang()
        isigrid()
    End Sub

    Sub isigrid()
        bukaDB()
        DA = New SqlDataAdapter("select * from [penumpang]", konek)
        DS = New DataSet
        DS.Clear()
        DA.Fill(DS, "penumpang")
        DataGridView1.DataSource = (DS.Tables("penumpang"))
        DataGridView1.Enabled = True
    End Sub

    Sub bersih()
        idpenumpang()
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""

    End Sub

    Private Sub jalankansql(ByVal sQL As String)
        Dim objcmd As New System.Data.SqlClient.SqlCommand
        Try
            objcmd.Connection = konek
            objcmd.CommandType = CommandType.Text
            objcmd.CommandText = sQL
            objcmd.ExecuteNonQuery()
            objcmd.Dispose()
            MsgBox("Data Berhasil di simpan!!", vbInformation)
        Catch ex As Exception
            MsgBox("Data Gagal disimpan!!" & ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        databaru = True

        Dim jk = jeniskelamin()

        Dim pesan As String
        Dim simpan As String

        If TextBox2.Text = "" And TextBox3.Text = "" Then Exit Sub

        If databaru Then
            pesan = MsgBox("Apakah anda yakin akan menambah data ke database??", vbYesNo + vbQuestion)
            If pesan = vbYesNo Then
                Exit Sub
            End If
            simpan = "insert into [penumpang](id_penumpang,nama_penumpang,no_identitas,alamat,jenis_kelamin,email,no_tlp) values ('" & TextBox1.Text & "','" & TextBox2.Text & "','" & TextBox3.Text & "','" & TextBox4.Text & "','" & jk & "','" & TextBox5.Text & "','" & TextBox6.Text & "')"
            bersih()

        Else
            pesan = MsgBox("Apakah anda yakin akan mengupdate database??", vbYesNo + vbQuestion)
            If pesan = vbYesNo Then
                Exit Sub
            End If
            simpan = "update [penumpang] set " _
                + "nama_penumpang ='" & TextBox2.Text & "'," _
                + "no_identitas ='" & TextBox3.Text & "'," _
                + "alamat ='" & TextBox4.Text & "'," _
                + "jenis_kelamin ='" & jk & "'," _
                + "email ='" & TextBox5.Text & "'," _
                + "no_tlp='" & TextBox6.Text & "' where id_penumpang='" & TextBox1.Text & "'"
            bersih()

        End If
        jalankansql(simpan)
        DataGridView1.Refresh()
        isigrid()
        idpenumpang()
    End Sub


    Private Sub GantiPasswordToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GantiPasswordToolStripMenuItem.Click
        FormGantiPassword.Show()
    End Sub

    Private Sub TambahKeretaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahKeretaToolStripMenuItem.Click
        FormTambahKereta.Show()
        Me.Dispose()
    End Sub

    Private Sub TambahStasiunToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahStasiunToolStripMenuItem.Click
        FormTambahStasiun.Show()
        Me.Dispose()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        FormTambahUser.Show()
        Me.Dispose()

    End Sub

    Private Sub TambahJadwalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahJadwalToolStripMenuItem.Click
        FormTambahJadwal.Show()
        Me.Dispose()
    End Sub

    Private Sub DataPenumpangToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DataPenumpangToolStripMenuItem.Click
        Me.Show()
    End Sub

    Private Sub TransaksiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TransaksiToolStripMenuItem.Click
        FormTransaksi.Show()
        Me.Dispose()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim jk = jeniskelamin()

        Dim pesan As String
        Dim update As String

        pesan = MsgBox("Apakah anda yakin akan mengupdate database??", vbYesNo + vbQuestion)
        If pesan = vbYesNo Then
            Exit Sub
        End If
        update = "update [penumpang] set " _
            + "nama_penumpang ='" & TextBox2.Text & "'," _
            + "no_identitas ='" & TextBox3.Text & "'," _
            + "alamat ='" & TextBox4.Text & "'," _
            + "jenis_kelamin ='" & jk & "'," _
            + "email ='" & TextBox5.Text & "'," _
            + "no_tlp='" & TextBox6.Text & "' where id_penumpang='" & TextBox1.Text & "'"
        bersih()

        jalankansql(update)
        DataGridView1.Refresh()
        isigrid()
        idpenumpang()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        bersih()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim hapussql As String
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan nenghapus data dari server... '" & TextBox2.Text & "' ", vbYesNo + vbExclamation, "PERHATIAN!!")
        hapussql = "delete from [penumpang] where id_penumpang='" & TextBox1.Text & "'"

        jalankansql(hapussql)
        DataGridView1.Refresh()
        isigrid()
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        isitextbox(e.RowIndex)
    End Sub

    Private Sub BulananToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BulananToolStripMenuItem.Click
        FormLaporan.Show()
    End Sub

    Private Sub LogOutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LogOutToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan Log Out?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            FormLogin.Show()
            Me.Dispose()
        End If
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KeluarToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan keluar?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            Me.Close()
        End If
    End Sub
End Class