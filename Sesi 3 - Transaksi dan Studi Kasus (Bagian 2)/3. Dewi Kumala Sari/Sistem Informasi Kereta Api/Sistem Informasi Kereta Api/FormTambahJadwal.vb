﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FormTambahJadwal

    Private Sub GantiPasswordToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GantiPasswordToolStripMenuItem.Click
        FormGantiPassword.Show()
    End Sub

    Private Sub TambahKeretaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahKeretaToolStripMenuItem.Click
        FormTambahKereta.Show()
        Me.Dispose()
    End Sub

    Private Sub TambahStasiunToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahStasiunToolStripMenuItem.Click
        FormTambahStasiun.Show()
        Me.Dispose()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        FormTambahUser.Show()
        Me.Dispose()

    End Sub

    Private Sub TambahJadwalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahJadwalToolStripMenuItem.Click
        Me.Show()
    End Sub

    Private Sub DataPenumpangToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DataPenumpangToolStripMenuItem.Click
        FormTambahPenumpang.Show()
        Me.Close()
    End Sub

    Private Sub TransaksiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TransaksiToolStripMenuItem.Click
        FormTransaksi.Show()
        Me.Dispose()

    End Sub

    Dim databaru As Boolean

    Sub idjadwal()
        bukaDB()
        TextBox1.Enabled = False
        Dim nilai1 As String = ""
        Dim nilai2 As String = ""
        CMD = New SqlCommand("select * from [jadwal] order by id_jadwal desc", konek)
        DR = CMD.ExecuteReader
        If DR.Read() Then
            nilai1 = Mid(DR.Item("id_jadwal"), 3, 2)
            nilai2 = Val(nilai1) + 1
            TextBox1.Text = "J-" + Mid("0", 1, 2 - nilai2.Length) & nilai2
        Else
            TextBox1.Text = "J-01"
        End If
        TextBox2.Focus()
    End Sub

    Sub isigrid()
        bukaDB()
        DA = New SqlDataAdapter("select * from [jadwal]", konek)
        DS = New DataSet
        DS.Clear()
        DA.Fill(DS, "jadwal")
        DataGridView1.DataSource = (DS.Tables("jadwal"))
        DataGridView1.Enabled = True
    End Sub

    Private Sub jalankansql(ByVal sQL As String)
        Dim objcmd As New System.Data.SqlClient.SqlCommand
        Try
            objcmd.Connection = konek
            objcmd.CommandType = CommandType.Text
            objcmd.CommandText = sQL
            objcmd.ExecuteNonQuery()
            objcmd.Dispose()
            MsgBox("Data Berhasil di simpan!!", vbInformation)
        Catch ex As Exception
            MsgBox("Data Gagal disimpan!!" & ex.Message)
        End Try
    End Sub

    Sub bersih()
        idjadwal()
        ComboBox4.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        ComboBox5.Text = ""
    End Sub

    Private Sub FormTambahJadwal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bukaDB()
        idjadwal()
        isigrid()

        CMD = New SqlCommand("SELECT * from [stasiun]", konek)
        DR = CMD.ExecuteReader
        While DR.Read
            ComboBox2.Items.Add(DR.Item("nama_stasiun"))
            ComboBox3.Items.Add(DR.Item("nama_stasiun"))
            ComboBox4.Items.Add(DR.Item("nama_stasiun"))
            ComboBox6.Items.Add(DR.Item("nama_stasiun"))
            ComboBox7.Items.Add(DR.Item("nama_stasiun"))
        End While

        Dim CMD1 = New SqlCommand("SELECT * from [kereta]", konek)
        Dim DR1 = CMD1.ExecuteReader
        While DR1.Read
            ComboBox1.Items.Add(DR1.Item("nama_kereta"))
        End While

        TextBox2.Enabled = False
        TextBox3.Enabled = False
        TextBox4.Enabled = False
        TextBox5.Enabled = False
        TextBox6.Enabled = False
        TextBox7.Enabled = False
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        CMD = New SqlCommand("SELECT id_kereta from [kereta] where nama_kereta = '" & ComboBox1.SelectedItem & "'", konek)
        Dim DR As Integer = CMD.ExecuteScalar

        Dim CMD1 = New SqlCommand("SELECT SUM(IIF(kelas like 'ekonomi%',jumlah_kursi,0)) As hasil from [gerbong] where id_kereta='" & DR & "'", konek)
        Dim DR1 = CMD1.ExecuteReader
        If DR1.HasRows Then
            If DR1.Read Then
                TextBox2.Text = DR1.Item("hasil")
            End If
        End If

        Dim CMD2 = New SqlCommand("SELECT SUM(IIF(kelas like 'bisnis%',jumlah_kursi,0)) As hasil from [gerbong] where id_kereta='" & DR & "'", konek)
        Dim DR2 = CMD2.ExecuteReader
        If DR2.HasRows Then
            If DR2.Read Then
                TextBox3.Text = DR2.Item("hasil")
            End If
        End If

        Dim CMD3 = New SqlCommand("SELECT SUM(IIF(kelas like 'eksekutif%',jumlah_kursi,0)) As hasil from [gerbong] where id_kereta='" & DR & "'", konek)
        Dim DR3 = CMD3.ExecuteReader
        If DR3.HasRows Then
            If DR3.Read Then
                TextBox4.Text = DR3.Item("hasil")
            End If
        End If

        Dim CMD4 = New SqlCommand("SELECT * from [gerbong] where id_kereta='" & DR & "' and kelas='Ekonomi'", konek)
        Dim DR4 = CMD4.ExecuteReader
        If DR4.HasRows Then
            If DR4.Read Then
                TextBox5.Text = DR4.Item("harga")
            End If
        End If

        Dim CMD5 = New SqlCommand("SELECT * from [gerbong] where id_kereta='" & DR & "' and kelas='Bisnis'", konek)
        Dim DR5 = CMD5.ExecuteReader
        If DR5.HasRows Then
            If DR5.Read Then
                TextBox6.Text = DR5.Item("harga")
            End If
        End If

        Dim CMD6 = New SqlCommand("SELECT * from [gerbong] where id_kereta='" & DR & "' and kelas='Eksekutif'", konek)
        Dim DR6 = CMD6.ExecuteReader
        If DR6.HasRows Then
            If DR6.Read Then
                TextBox7.Text = DR6.Item("harga")
            End If
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        databaru = True
        CMD = New SqlCommand("SELECT id_kereta from [kereta] where nama_kereta = '" & ComboBox1.SelectedItem & "'", konek)
        Dim DR As Integer = CMD.ExecuteScalar

        Dim simpan As String
        Dim pesan As String
        Dim date1, tgl1, bln1, thn1, jam1, mnt1, dtk1 As String
        Dim date2, tgl2, bln2, thn2, jam2, mnt2, dtk2 As String

        thn1 = DateTimePicker1.Value.Year
        bln1 = DateTimePicker1.Value.Month
        tgl1 = DateTimePicker1.Value.Day
        jam1 = DateTimePicker1.Value.Hour
        mnt1 = DateTimePicker1.Value.Minute
        dtk1 = DateTimePicker1.Value.Second
        thn2 = DateTimePicker2.Value.Year
        bln2 = DateTimePicker2.Value.Month
        tgl2 = DateTimePicker2.Value.Day
        jam2 = DateTimePicker2.Value.Hour
        mnt2 = DateTimePicker2.Value.Minute
        dtk2 = DateTimePicker2.Value.Second

        date1 = thn1 & "-" & bln1 & "-" & tgl1 & " " & jam1 & ":" & mnt1 & ":" & dtk1
        date2 = thn2 & "-" & bln2 & "-" & tgl2 & " " & jam2 & ":" & mnt2 & ":" & dtk2

        If TextBox2.Text = "" And TextBox2.Text = "" Then Exit Sub

        If databaru Then
            pesan = MsgBox("Apakah anda yakin akan menambah data ke database??", vbYesNo + vbQuestion)
            If pesan = vbYesNo Then
                Exit Sub
            End If
            simpan = "insert into [jadwal](id_jadwal,id_kereta,stasiun_awal,stasiun_tujuan,stasiun_berhenti,waktu_berangkat,waktu_datang,kursi_ekonomi,kursi_bisnis,kursi_eksekutif,harga_ekonomi,harga_bisnis,harga_eksekutif,status) values ('" & TextBox1.Text & "','" & DR & "','" & ComboBox2.SelectedItem & "','" & ComboBox3.SelectedItem & "','" & ComboBox4.SelectedItem & "','" & date1 & "','" & date2 & "','" & TextBox2.Text & "','" & TextBox3.Text & "','" & TextBox4.Text & "','" & TextBox5.Text & "','" & TextBox6.Text & "','" & TextBox7.Text & "','" & ComboBox5.SelectedItem & "')"
            bersih()
        Else
            pesan = MsgBox("Apakah anda yakin akan mengupdate database??", vbYesNo + vbQuestion)
            If pesan = vbYesNo Then
                Exit Sub
            End If
            simpan = "update [jadwal] set " _
                + "id_stasiun ='" & DR & "'," _
                + "stasiun_awal ='" & ComboBox2.SelectedItem & "'," _
                + "stasiun_tujuan ='" & ComboBox3.SelectedItem & "'," _
                + "stasiun_berhenti ='" & ComboBox4.SelectedItem & "'," _
                + "waktu_berangkat ='" & date1 & "'," _
                + "waktu_datang ='" & date2 & "'," _
                + "kursi_ekonomi ='" & TextBox2.Text & "'," _
                + "kursi_bisnis ='" & TextBox3.Text & "'," _
                + "kursi_eksekutif ='" & TextBox4.Text & "'," _
                + "harga_ekonomi ='" & TextBox5.Text & "'," _
                + "harga_bisnis ='" & TextBox6.Text & "'," _
                + "harga_eksekutif ='" & TextBox7.Text & "'," _
                + "status='" & ComboBox5.SelectedItem & "' where id_jadwal='" & TextBox1.Text & "'"
            bersih()
        End If
        jalankansql(simpan)
        DataGridView1.Refresh()
        isigrid()
        idjadwal()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        bersih()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        CMD = New SqlCommand("SELECT id_kereta from [kereta] where nama_kereta = '" & ComboBox1.SelectedItem & "'", konek)
        Dim DR As Integer = CMD.ExecuteScalar

        Dim pesan As String
        Dim update As String
        Dim date1, tgl1, bln1, thn1, jam1, mnt1, dtk1 As String
        Dim date2, tgl2, bln2, thn2, jam2, mnt2, dtk2 As String

        thn1 = DateTimePicker1.Value.Year
        bln1 = DateTimePicker1.Value.Month
        tgl1 = DateTimePicker1.Value.Day
        jam1 = DateTimePicker1.Value.Hour
        mnt1 = DateTimePicker1.Value.Minute
        dtk1 = DateTimePicker1.Value.Second
        thn2 = DateTimePicker2.Value.Year
        bln2 = DateTimePicker2.Value.Month
        tgl2 = DateTimePicker2.Value.Day
        jam2 = DateTimePicker2.Value.Hour
        mnt2 = DateTimePicker2.Value.Minute
        dtk2 = DateTimePicker2.Value.Second

        date1 = thn1 & "-" & bln1 & "-" & tgl1 & " " & jam1 & ":" & mnt1 & ":" & dtk1
        date2 = thn2 & "-" & bln2 & "-" & tgl2 & " " & jam2 & ":" & mnt2 & ":" & dtk2



        pesan = MsgBox("Apakah anda yakin akan mengupdate database??", vbYesNo + vbQuestion)

        If pesan = vbYesNo Then
            Exit Sub
        End If

        update = "update [jadwal] set " _
            + "id_stasiun ='" & DR & "'," _
            + "stasiun_awal ='" & ComboBox2.SelectedItem & "'," _
            + "stasiun_tujuan ='" & ComboBox3.SelectedItem & "'," _
            + "stasiun_berhenti ='" & ComboBox4.SelectedItem & "'," _
            + "waktu_berangkat ='" & date1 & "'," _
            + "waktu_datang ='" & date2 & "'," _
            + "kursi_ekonomi ='" & TextBox2.Text & "'," _
            + "kursi_bisnis ='" & TextBox3.Text & "'," _
            + "kursi_eksekutif ='" & TextBox4.Text & "'," _
            + "harga_ekonomi ='" & TextBox5.Text & "'," _
            + "harga_bisnis ='" & TextBox6.Text & "'," _
            + "harga_eksekutif ='" & TextBox7.Text & "'," _
            + "status='" & ComboBox5.SelectedItem & "' where id_jadwal='" & TextBox1.Text & "'"
        bersih()

        ComboBox1.Text = ""
        ComboBox2.Text = ""
        ComboBox3.Text = ""

        jalankansql(update)
        DataGridView1.Refresh()
        isigrid()
        idjadwal()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim hapussql As String
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan nenghapus data dari server... '" & TextBox2.Text & "' ", vbYesNo + vbExclamation, "PERHATIAN!!")
        hapussql = "delete from [jadwal] where id_jadwal='" & TextBox1.Text & "'"

        jalankansql(hapussql)
        DataGridView1.Refresh()
        isigrid()
    End Sub

    Private Sub BulananToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BulananToolStripMenuItem.Click
        FormLaporan.Show()
    End Sub

    Private Sub LogOutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LogOutToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan Log Out?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            FormLogin.Show()
            Me.Dispose()
        End If
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KeluarToolStripMenuItem.Click
        Dim pesan As String
        pesan = MsgBox("Apakah anda yakin akan keluar?", vbYesNo + vbCritical, "Perhatian!!!")
        If pesan = vbYes Then
            Me.Close()
        End If
    End Sub


    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        DA = New SqlDataAdapter("select stasiun_berhenti from jadwal where stasiun_awal like '%" & ComboBox6.SelectedItem & "' and stasiun_tujuan like'%" & ComboBox7.SelectedItem & "'", konek)
        Dim srt = New DataTable
        srt.Clear()
        DA.Fill(srt)
        DataGridView1.DataSource = srt
        DataGridView1.Refresh()
    End Sub
End Class