USE [master]
GO
/****** Object:  Database [tiket]    Script Date: 9/11/2016 5:59:22 AM ******/
CREATE DATABASE [tiket]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'tiket', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\tiket.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'tiket_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\tiket_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [tiket] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [tiket].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [tiket] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [tiket] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [tiket] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [tiket] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [tiket] SET ARITHABORT OFF 
GO
ALTER DATABASE [tiket] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [tiket] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [tiket] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [tiket] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [tiket] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [tiket] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [tiket] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [tiket] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [tiket] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [tiket] SET  DISABLE_BROKER 
GO
ALTER DATABASE [tiket] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [tiket] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [tiket] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [tiket] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [tiket] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [tiket] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [tiket] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [tiket] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [tiket] SET  MULTI_USER 
GO
ALTER DATABASE [tiket] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [tiket] SET DB_CHAINING OFF 
GO
ALTER DATABASE [tiket] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [tiket] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [tiket] SET DELAYED_DURABILITY = DISABLED 
GO
USE [tiket]
GO
/****** Object:  Table [dbo].[kereta]    Script Date: 9/11/2016 5:59:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[kereta](
	[nama_ka] [varchar](50) NOT NULL,
	[tujuan] [varchar](50) NOT NULL,
	[waktu_berangkat] [int] NOT NULL,
	[waktu_tiba] [int] NOT NULL,
 CONSTRAINT [PK_kereta] PRIMARY KEY CLUSTERED 
(
	[nama_ka] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pembayaran]    Script Date: 9/11/2016 5:59:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pembayaran](
	[harga_dan_nomor_id] [varchar](50) NOT NULL,
	[jumlah_uang] [int] NOT NULL,
	[kembalian] [int] NOT NULL,
 CONSTRAINT [PK_pembayaran] PRIMARY KEY CLUSTERED 
(
	[harga_dan_nomor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[penjadwalan]    Script Date: 9/11/2016 5:59:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[penjadwalan](
	[nama_ka] [varchar](20) NOT NULL,
	[waktu_datang] [int] NOT NULL,
	[waktu_tiba] [int] NOT NULL,
	[rute] [varchar](50) NOT NULL,
	[kapasitas_kursi] [int] NOT NULL,
	[clas_dan_harga] [varchar](20) NOT NULL,
	[urutan_pemberhentian] [varchar](50) NOT NULL,
 CONSTRAINT [PK_penjadwalan_1] PRIMARY KEY CLUSTERED 
(
	[nama_ka] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tiketing]    Script Date: 9/11/2016 5:59:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tiketing](
	[nama_lengkap] [varchar](50) NOT NULL,
	[nomor_id] [varchar](5) NOT NULL,
	[alamat] [varchar](50) NOT NULL,
	[jenis_kelamin] [varchar](5) NOT NULL,
	[email_dan_nomor.hp] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tiketing] PRIMARY KEY CLUSTERED 
(
	[nama_lengkap] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[kereta] ([nama_ka], [tujuan], [waktu_berangkat], [waktu_tiba]) VALUES (N'Argo Lawu', N'Blitar', 10, 12)
INSERT [dbo].[pembayaran] ([harga_dan_nomor_id], [jumlah_uang], [kembalian]) VALUES (N'80000 KA001', 100000, 20000)
INSERT [dbo].[penjadwalan] ([nama_ka], [waktu_datang], [waktu_tiba], [rute], [kapasitas_kursi], [clas_dan_harga], [urutan_pemberhentian]) VALUES (N'KA Argo Lawu', 10, 12, N'ponorogo-blitar', 100, N'economy 10.000', N'ponorogo-trenggalek-tulung agung-blitar')
INSERT [dbo].[tiketing] ([nama_lengkap], [nomor_id], [alamat], [jenis_kelamin], [email_dan_nomor.hp]) VALUES (N'Nur Muhammad Fadhilah', N'KA001', N'ds/kec badegan kab.PONOROGO', N'L', N'nurmuh.fadil@gmail.com 087765654567')
USE [master]
GO
ALTER DATABASE [tiket] SET  READ_WRITE 
GO
