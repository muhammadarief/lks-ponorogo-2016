USE [master]
GO
/****** Object:  Database [db_kereta]    Script Date: 9/11/2016 6:03:42 AM ******/
CREATE DATABASE [db_kereta]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_kereta', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.DEWI\MSSQL\DATA\db_kereta.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'db_kereta_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.DEWI\MSSQL\DATA\db_kereta_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [db_kereta] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_kereta].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_kereta] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_kereta] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_kereta] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_kereta] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_kereta] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_kereta] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_kereta] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_kereta] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_kereta] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_kereta] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_kereta] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_kereta] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_kereta] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_kereta] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_kereta] SET  DISABLE_BROKER 
GO
ALTER DATABASE [db_kereta] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_kereta] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_kereta] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_kereta] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_kereta] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_kereta] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_kereta] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_kereta] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [db_kereta] SET  MULTI_USER 
GO
ALTER DATABASE [db_kereta] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_kereta] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_kereta] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_kereta] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [db_kereta] SET DELAYED_DURABILITY = DISABLED 
GO
USE [db_kereta]
GO
/****** Object:  Table [dbo].[akses]    Script Date: 9/11/2016 6:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[akses](
	[id_akses] [int] NOT NULL,
	[status] [varchar](50) NULL,
 CONSTRAINT [PK_akses] PRIMARY KEY CLUSTERED 
(
	[id_akses] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gerbong]    Script Date: 9/11/2016 6:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gerbong](
	[id_gerbong] [int] IDENTITY(1,1) NOT NULL,
	[id_kereta] [int] NULL,
	[no_gerbong] [int] NULL,
	[kelas] [varchar](30) NULL,
	[jumlah_kursi] [int] NULL,
	[harga] [float] NULL,
 CONSTRAINT [PK_gerbong] PRIMARY KEY CLUSTERED 
(
	[id_gerbong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[jadwal]    Script Date: 9/11/2016 6:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[jadwal](
	[id_jadwal] [varchar](20) NOT NULL,
	[id_kereta] [int] NULL,
	[stasiun_awal] [varchar](50) NULL,
	[stasiun_tujuan] [varchar](50) NULL,
	[stasiun_berhenti] [varchar](50) NULL,
	[waktu_berangkat] [datetime] NULL,
	[waktu_datang] [datetime] NULL,
	[kursi_ekonomi] [int] NULL,
	[kursi_bisnis] [int] NULL,
	[kursi_eksekutif] [int] NULL,
	[harga_ekonomi] [float] NULL,
	[harga_bisnis] [float] NOT NULL,
	[harga_eksekutif] [float] NULL,
	[status] [varchar](20) NULL,
 CONSTRAINT [PK_jadwal] PRIMARY KEY CLUSTERED 
(
	[id_jadwal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[kereta]    Script Date: 9/11/2016 6:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[kereta](
	[id_kereta] [int] IDENTITY(1,1) NOT NULL,
	[nama_kereta] [varchar](50) NULL,
	[jumlah_gerbong] [int] NULL,
 CONSTRAINT [PK_kereta] PRIMARY KEY CLUSTERED 
(
	[id_kereta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[kursi]    Script Date: 9/11/2016 6:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[kursi](
	[id_kursi] [int] IDENTITY(1,1) NOT NULL,
	[id_kereta] [int] NULL,
	[id_gerbong] [int] NULL,
	[no_kursi] [int] NULL,
	[status_kursi] [int] NULL,
 CONSTRAINT [PK_kursi] PRIMARY KEY CLUSTERED 
(
	[id_kursi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[pegawai]    Script Date: 9/11/2016 6:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pegawai](
	[id_pegawai] [varchar](20) NOT NULL,
	[nama_pegawai] [varchar](20) NULL,
	[tempat_lahir] [varchar](25) NULL,
	[tanggal_lahir] [date] NULL,
	[alamat] [varchar](75) NULL,
	[no_tlp] [varchar](20) NULL,
	[email] [varchar](50) NULL,
	[stasiun_bekerja] [varchar](50) NULL,
 CONSTRAINT [PK_pegawai] PRIMARY KEY CLUSTERED 
(
	[id_pegawai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[penumpang]    Script Date: 9/11/2016 6:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[penumpang](
	[id_penumpang] [varchar](20) NOT NULL,
	[nama_penumpang] [varchar](50) NULL,
	[no_identitas] [int] NULL,
	[alamat] [varchar](50) NULL,
	[jenis_kelamin] [varchar](20) NULL,
	[email] [varchar](50) NULL,
	[no_tlp] [varchar](25) NULL,
 CONSTRAINT [PK_penumpang] PRIMARY KEY CLUSTERED 
(
	[id_penumpang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[stasiun]    Script Date: 9/11/2016 6:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[stasiun](
	[id_stasiun] [varchar](20) NOT NULL,
	[nama_stasiun] [varchar](50) NULL,
	[kota] [varchar](50) NULL,
	[provinsi] [varchar](50) NULL,
 CONSTRAINT [PK_stasiun] PRIMARY KEY CLUSTERED 
(
	[id_stasiun] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[transaksi]    Script Date: 9/11/2016 6:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transaksi](
	[id_transaksi] [varchar](20) NOT NULL,
	[id_penumpang] [varchar](20) NULL,
	[id_user] [varchar](20) NULL,
	[id_kereta] [int] NULL,
	[stasiun_awal] [varchar](50) NULL,
	[stasiun_berhenti] [varchar](50) NULL,
	[waktu_berangkat] [datetime] NULL,
	[waktu_datang] [datetime] NULL,
	[kelas] [varchar](20) NULL,
	[no_gerbong] [int] NULL,
	[no_kursi] [int] NULL,
	[harga] [float] NULL,
	[tgl_beli] [date] NULL,
 CONSTRAINT [PK_transaksi] PRIMARY KEY CLUSTERED 
(
	[id_transaksi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user]    Script Date: 9/11/2016 6:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user](
	[id_user] [varchar](20) NOT NULL,
	[id_pegawai] [varchar](20) NULL,
	[id_akses] [int] NULL,
	[username] [varchar](50) NULL,
	[password] [varchar](50) NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[id_user] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[v_akun]    Script Date: 9/11/2016 6:03:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_akun]
AS
SELECT        dbo.pegawai.id_pegawai, dbo.pegawai.nama_pegawai, dbo.pegawai.tempat_lahir, dbo.pegawai.tanggal_lahir, dbo.pegawai.alamat, dbo.pegawai.no_tlp, 
                         dbo.pegawai.email, dbo.pegawai.stasiun_bekerja, dbo.akses.status, dbo.[user].username, dbo.[user].password
FROM            dbo.akses INNER JOIN
                         dbo.[user] ON dbo.akses.id_akses = dbo.[user].id_akses INNER JOIN
                         dbo.pegawai ON dbo.[user].id_pegawai = dbo.pegawai.id_pegawai

GO
INSERT [dbo].[akses] ([id_akses], [status]) VALUES (1, N'Admin')
INSERT [dbo].[akses] ([id_akses], [status]) VALUES (2, N'Penjadwalan')
INSERT [dbo].[akses] ([id_akses], [status]) VALUES (3, N'Tiketing')
INSERT [dbo].[akses] ([id_akses], [status]) VALUES (4, N'Boarding')
INSERT [dbo].[akses] ([id_akses], [status]) VALUES (5, N'Operasi')
SET IDENTITY_INSERT [dbo].[gerbong] ON 

INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (2, 2, 1, N'Ekonomi', 10, 125000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (3, 2, 2, N'Ekonomi', 10, 125000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (4, 2, 3, N'Ekonomi', 10, 125000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (5, 2, 4, N'Ekonomi', 10, 125000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (6, 2, 5, N'Bisnis', 5, 150000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (7, 2, 6, N'Bisnis', 5, 150000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (8, 2, 7, N'Bisnis', 5, 150000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (9, 2, 8, N'Bisnis', 5, 150000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (10, 2, 9, N'Eksekutif', 4, 175000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (11, 2, 10, N'Eksekutif', 4, 175000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (12, 2, 11, N'Eksekutif', 4, 175000)
INSERT [dbo].[gerbong] ([id_gerbong], [id_kereta], [no_gerbong], [kelas], [jumlah_kursi], [harga]) VALUES (13, 2, 12, N'Eksekutif', 4, 175000)
SET IDENTITY_INSERT [dbo].[gerbong] OFF
SET IDENTITY_INSERT [dbo].[kereta] ON 

INSERT [dbo].[kereta] ([id_kereta], [nama_kereta], [jumlah_gerbong]) VALUES (1, N'Gajayana', 9)
INSERT [dbo].[kereta] ([id_kereta], [nama_kereta], [jumlah_gerbong]) VALUES (2, N'Bima', 12)
SET IDENTITY_INSERT [dbo].[kereta] OFF
SET IDENTITY_INSERT [dbo].[kursi] ON 

INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (1, 2, 2, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (2, 2, 2, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (3, 2, 2, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (4, 2, 2, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (5, 2, 2, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (6, 2, 2, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (7, 2, 2, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (8, 2, 2, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (9, 2, 2, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (10, 2, 2, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (11, 2, 3, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (12, 2, 3, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (13, 2, 3, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (14, 2, 3, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (15, 2, 3, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (16, 2, 3, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (17, 2, 3, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (18, 2, 3, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (19, 2, 3, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (20, 2, 3, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (21, 2, 4, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (22, 2, 4, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (23, 2, 4, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (24, 2, 4, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (25, 2, 4, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (26, 2, 4, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (27, 2, 4, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (28, 2, 4, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (29, 2, 4, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (30, 2, 4, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (31, 2, 5, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (32, 2, 5, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (33, 2, 5, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (34, 2, 5, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (35, 2, 5, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (36, 2, 5, 6, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (37, 2, 5, 7, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (38, 2, 5, 8, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (39, 2, 5, 9, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (40, 2, 5, 10, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (41, 2, 6, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (42, 2, 6, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (43, 2, 6, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (44, 2, 6, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (45, 2, 6, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (46, 2, 7, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (47, 2, 7, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (48, 2, 7, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (49, 2, 7, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (50, 2, 7, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (51, 2, 8, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (52, 2, 8, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (53, 2, 8, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (54, 2, 8, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (55, 2, 8, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (56, 2, 9, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (57, 2, 9, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (58, 2, 9, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (59, 2, 9, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (60, 2, 9, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (61, 2, 10, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (62, 2, 10, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (63, 2, 10, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (64, 2, 10, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (65, 2, 10, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (66, 2, 11, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (67, 2, 11, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (68, 2, 11, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (69, 2, 11, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (70, 2, 11, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (71, 2, 12, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (72, 2, 12, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (73, 2, 12, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (74, 2, 12, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (75, 2, 12, 5, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (76, 2, 13, 1, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (77, 2, 13, 2, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (78, 2, 13, 3, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (79, 2, 13, 4, 0)
INSERT [dbo].[kursi] ([id_kursi], [id_kereta], [id_gerbong], [no_kursi], [status_kursi]) VALUES (80, 2, 13, 5, 0)
SET IDENTITY_INSERT [dbo].[kursi] OFF
INSERT [dbo].[pegawai] ([id_pegawai], [nama_pegawai], [tempat_lahir], [tanggal_lahir], [alamat], [no_tlp], [email], [stasiun_bekerja]) VALUES (N'PG-01', N'Admin', N'Ponorogo', CAST(N'1998-06-30' AS Date), N'Ponorogo', N'083845338679', N'Admin@gmail.com', N'Gambir')
INSERT [dbo].[stasiun] ([id_stasiun], [nama_stasiun], [kota], [provinsi]) VALUES (N'ST-01', N'Gambir', N'Jakarta', N'DKI Jakarta')
INSERT [dbo].[user] ([id_user], [id_pegawai], [id_akses], [username], [password]) VALUES (N'U01', N'PG-01', 1, N'Admin', N'admin')
ALTER TABLE [dbo].[gerbong]  WITH CHECK ADD  CONSTRAINT [FK_gerbong_kereta] FOREIGN KEY([id_kereta])
REFERENCES [dbo].[kereta] ([id_kereta])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[gerbong] CHECK CONSTRAINT [FK_gerbong_kereta]
GO
ALTER TABLE [dbo].[jadwal]  WITH CHECK ADD  CONSTRAINT [FK_jadwal_kereta] FOREIGN KEY([id_kereta])
REFERENCES [dbo].[kereta] ([id_kereta])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[jadwal] CHECK CONSTRAINT [FK_jadwal_kereta]
GO
ALTER TABLE [dbo].[kursi]  WITH CHECK ADD  CONSTRAINT [FK_kursi_gerbong] FOREIGN KEY([id_gerbong])
REFERENCES [dbo].[gerbong] ([id_gerbong])
GO
ALTER TABLE [dbo].[kursi] CHECK CONSTRAINT [FK_kursi_gerbong]
GO
ALTER TABLE [dbo].[kursi]  WITH CHECK ADD  CONSTRAINT [FK_kursi_kereta] FOREIGN KEY([id_kereta])
REFERENCES [dbo].[kereta] ([id_kereta])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[kursi] CHECK CONSTRAINT [FK_kursi_kereta]
GO
ALTER TABLE [dbo].[transaksi]  WITH CHECK ADD  CONSTRAINT [FK_transaksi_kereta] FOREIGN KEY([id_kereta])
REFERENCES [dbo].[kereta] ([id_kereta])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[transaksi] CHECK CONSTRAINT [FK_transaksi_kereta]
GO
ALTER TABLE [dbo].[transaksi]  WITH CHECK ADD  CONSTRAINT [FK_transaksi_penumpang] FOREIGN KEY([id_penumpang])
REFERENCES [dbo].[penumpang] ([id_penumpang])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[transaksi] CHECK CONSTRAINT [FK_transaksi_penumpang]
GO
ALTER TABLE [dbo].[transaksi]  WITH CHECK ADD  CONSTRAINT [FK_transaksi_user] FOREIGN KEY([id_user])
REFERENCES [dbo].[user] ([id_user])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[transaksi] CHECK CONSTRAINT [FK_transaksi_user]
GO
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [FK_user_akses] FOREIGN KEY([id_akses])
REFERENCES [dbo].[akses] ([id_akses])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [FK_user_akses]
GO
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [FK_user_pegawai] FOREIGN KEY([id_pegawai])
REFERENCES [dbo].[pegawai] ([id_pegawai])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [FK_user_pegawai]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "akses"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 120
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "user"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pegawai"
            Begin Extent = 
               Top = 6
               Left = 454
               Bottom = 155
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_akun'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_akun'
GO
USE [master]
GO
ALTER DATABASE [db_kereta] SET  READ_WRITE 
GO
