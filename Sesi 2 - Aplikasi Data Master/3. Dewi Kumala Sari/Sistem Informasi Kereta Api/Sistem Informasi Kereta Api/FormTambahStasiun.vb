﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FormTambahStasiun
    Dim databaru As Boolean

    Sub idstasiun()
        bukaDB()
        TextBox1.Enabled = False
        Dim nilai1 As String = ""
        Dim nilai2 As String = ""
        CMD = New SqlCommand("select * from [stasiun] order by id_stasiun desc", konek)
        DR = CMD.ExecuteReader
        If DR.Read() Then
            nilai1 = Mid(DR.Item("id_stasiun"), 4, 2)
            nilai2 = Val(nilai1) + 1
            TextBox1.Text = "ST-" + Mid("0", 1, 2 - nilai2.Length) & nilai2
        Else
            TextBox1.Text = "ST-01"
        End If
        TextBox2.Focus()
    End Sub

    Sub isigrid()
        bukaDB()
        DA = New SqlDataAdapter("select * from [stasiun]", konek)
        DS = New DataSet
        DS.Clear()
        DA.Fill(DS, "stasiun")
        DataGridView1.DataSource = (DS.Tables("stasiun"))
        DataGridView1.Enabled = True
    End Sub

    Private Sub FormTambahStasiun_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bukaDB()
        idstasiun()
        isigrid()

    End Sub


    Private Sub GantiPasswordToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GantiPasswordToolStripMenuItem.Click
        FormGantiPassword.Show()
    End Sub

    Private Sub TambahKeretaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahKeretaToolStripMenuItem.Click
        FormTambahKereta.Show()
        Me.Dispose()
    End Sub

    Private Sub TambahStasiunToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TambahStasiunToolStripMenuItem.Click
        Me.Show()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        FormTambahUser.Show()
        Me.Dispose()

    End Sub

    Private Sub jalankansql(ByVal sQL As String)
        Dim objcmd As New System.Data.SqlClient.SqlCommand
        Try
            objcmd.Connection = konek
            objcmd.CommandType = CommandType.Text
            objcmd.CommandText = sQL
            objcmd.ExecuteNonQuery()
            objcmd.Dispose()
            MsgBox("Data Berhasil di simpan!!", vbInformation)
        Catch ex As Exception
            MsgBox("Data Gagal disimpan!!" & ex.Message)
        End Try
    End Sub

    Sub bersih()
        idstasiun()
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        databaru = True
        Dim pesan As String
        Dim simpan As String

        If TextBox2.Text = "" And TextBox2.Text = "" Then Exit Sub

        If databaru Then
            pesan = MsgBox("Apakah anda yakin akan menambah data ke database??", vbYesNo + vbQuestion)
            If pesan = vbYesNo Then
                Exit Sub
            End If
            simpan = "insert into [stasiun](id_stasiun,nama_stasiun,kota,provinsi) values ('" & TextBox1.Text & "','" & TextBox2.Text & "','" & TextBox3.Text & "','" & TextBox4.Text & "')"
            bersih()

        Else
            pesan = MsgBox("Apakah anda yakin akan mengupdate database??", vbYesNo + vbQuestion)
            If pesan = vbYesNo Then
                Exit Sub
            End If
            simpan = "update [stasiun] set " _
                + "nama_stasiun ='" & TextBox2.Text & "'," _
                + "kota ='" & TextBox3.Text & "'," _
                + "provinsi='" & TextBox4.Text & "' where id_stasiun='" & TextBox1.Text & "'"
            bersih()
        End If
        jalankansql(simpan)
        DataGridView1.Refresh()
        isigrid()
        idstasiun()
    End Sub
End Class